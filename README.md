# Ygopro Expansion 
---

`Supports Koishipro core` → https://github.com/purerosefallen/ygopro

**To download** open cmd in your ygopro folder: `git clone https://bitbucket.org/LordOfNightmares/expansions.git`
> Required to have : https://git-scm.com/downloads

---
### Contents
- [1. Rulling](#markdown-header-1-rulling)
	- [1.1. Errata protocol](#markdown-header-11-errata-protocol)
- [2. New cards added](#markdown-header-2-new-cards-added)
- [3. Errata](#markdown-header-3-errata)
- [4. Unknown/TBD Errata](#markdown-header-4-unknowntbd-errata)
	- [4.1. Beta](#markdown-header-41-beta)
		- [4.1.1. Scripted](#markdown-header-411-scripted) (untested)
		- [4.1.2. Not yet Scripted](#markdown-header-412-not-yet-scripted)
	- [4.2. Unknown](#markdown-header-42-unknown)
		- [4.2.1. On Watch](#markdown-header-421-on-watch)
		- [4.2.2. Banlist](#markdown-header-422-banlist)
---

# 1. Rulling
---

| Master Rule 5.6 | 515959000 |
> 1: You can special summon monsters from Extra Deck _(except Link Monster(s))_ in all Monster Zones you control _(except Extra Monster Zone)_.  
2: You can special summon **Link Monster(s)** from Extra Deck only in **Extra Monster Zone** and zones where **Link Monster(s)** markers point to.  
3: If you control a **Link Monster(s)** you can only special summon monsters from Extra Deck only in **Extra Monster Zone** and zones where **Link Monster(s)** markers point to.  
4: Each turn, each player can negate activation of effects up to the number of their respective turns.  
• Each player may only negate activation of effects additionally by placing a card from their hand to the bottom of the deck.   
**Example**:  
player turn 1 - 0 free negations  
player turn 2+ - 1 free negations   
*Any consecutive negations after the `free negations` adds cost of sending an incremental amount of card(s) to the bottom of the deck from hand starting from 1.*    
5: During turn 1 before draw phase both players can shuffle up to 3 cards in their deck and draw the same amount of cards.   
6: If a player would deck out, at the start of their turn during draw phase shuffle all cards that are removed from play, from their gy, hand and faceup extra deck back into their deck and draw 6 cards; then while you are above 200 LP each consecutive card(s) added from their deck to their hand will inflict 200 LP damage.
7: 


## 1.1. Errata protocol
--- 

#### High Priority  
- No ftks/lockdowns *(everyone must play)*  
- No negation meta  
	> errata - cost for negation)  
- No bypassing the original summoning method  
- Prevent aoe  
	> errata - nerf full field wipes  
- Loop mechanics   
	> errata - to a linear decrease in gain	  
- No generic play  
	> errata - archtype lock  
- No obsolete cards  
	> errata - adjust new cards to have a nerfed different use.  

#### Medium Priority
- Stick to *anime/lore/card name or artowrk idea* when writting errata
- Minimize the amount of effects per card preferably to 2 (if able).

#### Low Priority
- Strategy archtype based errata:
	> errata - allow around 4 types of strategies if able.
- Attribute - Alpha ver.*NOT FINISHED, NEEDS FULL CONCEPTUAL ANALYSIS*
	- Dark - sp sum/grave/destruction
	- Fire - burn/desruction/lp manipulation
	- Water - lp manipulation/backrow removal
	- Wind - monster negation/return effs
	- Light - destruction/even the field
- Category - *NOT FINISHED, NEEDS FULL CONCEPTUAL ANALYSIS*
- Summon Type - *NOT FINISHED, NEEDS FULL CONCEPTUAL ANALYSIS*


# 2. New cards added
---

515958940 | D/D/D/D Super-Dimensional Sovereign Emperor Zero Paradox  
![D/D/D/D Super-Dimensional Sovereign Emperor Zero Paradox](/pics/515958940.png)  

**[ Pendulum Effect ]**  
> ①: This card's scale becomes the subtraction between both scales, you control.(This effect can not be negated.)     

**[ Monster Effect ]**  
> ①: This card cannot be Normal Summoned/Set.  
②: When you Pendulum Summon a "D/D/D" monster, and the combined value of your Pendulum Scales exceeds this card's level you can Special Summon this card from your Hand or Face-up Extra deck; then you can destroy all other cards on the field; any battle damage your opponent takes becomes halved for the rest of this turn.  
③: Once per turn, If a "D/D/D" card(s) you control is destroyed by battle or card effect; double this card's current ATK.  
④: Once per turn, during either player's turn, you can make this card gain the effects of a card in your opponent's Pendulum Zones until end phase.  
⑤: You can Pendulum Summon using your opponent's Pendulum Scales.  

515958941 | Starving Venemy Dragon  
![Starving Venemy Dragon](/pics/515958941.png)

**[ Pendulum Effect ]**  
> (Quick effect): Once per turn, you can reduce damage you take from one battle to zero, this effect cannot be negated. Each time a card(s) is sent form the field to the GY, place one Venemy Counter on a face-up monster your opponent controls.  

**[ Monster Effect ]**  
> 2 Dark Monsters  
(Quick Effect): Place 1 Venemy Counter on an opponent's monster, but you cannot activate this effect until the end of the next turn. This card gains the effects of monster(s) with Venemy or Predator Counter(s), except "Starving" monsters. Gains 100 ATK for each Venemy or Predator Counter on the field. If this card in the Monster Zone is destroyed by battle or card effect: You can place it in your Pendulum Zone.  


515958942 | Seraph Summoner  
![Seraph Summoner](/pics/515958942.png)  

**[ Monster Effect ]**   
> When this card is Summoned: You can Special Summon up to 2 other "Seraph Summoner" from your hand, Deck, and/or GY. You can only use this effect of "Seraph Summoner" once per turn. Cannot be Tributed, except for Tribute Summon of "The Wicked Eraser", "The Wicked Dreadroot", or "The Wicked Avatar". Once per turn, you can Tribute Summon "The Wicked Eraser", "The Wicked Dreadroot", or "The Wicked Avatar" from your GY. You cannot Special Summon monsters, except by the effect of "Seraph Summoner". If this card is destroyed by battle; destroy all "Seraph Summoner" you control.

515958943 | True Refined Ritual Art |  
![True Refined Ritual Art](/pics/515958943.png)  
**[ Spell Card ]**   
> This card is used to Ritual Summon any 1 Ritual Monster from your Deck. You must also Tribute Normal Monsters (except Tokens) from your hand or field whose Monster Type are the same and total Levels exactly equal the Level and Type of the Ritual Monster you Ritual Summon.

# 3. Erratas
---

10000020 | Slifer the Sky Dragon | 
> Requires 3 Tributes to Normal Summon (cannot be Normal Set). This card's Normal Summon cannot be negated. When Normal Summoned, cards and effects cannot be activated. The activation(s) and effect(s) of this card cannot be negated. Once per turn, during the End Phase, if this card was Special Summoned: Send it to the GY. Gains 1000 ATK and DEF for each card in your hand. If a monster(s) is Normal or Special Summoned to your opponent's field in Attack Position: That monster(s) loses 2000 ATK, then if its ATK has been reduced to 0 as a result, destroy it.

10000000 | Obelisk the Tormentor | 
> Requires 3 Tributes to Normal Summon (cannot be Normal Set). This card's Normal Summon cannot be negated. When Normal Summoned, cards and effects cannot be activated. The activation(s) and effect(s) of this card cannot be negated. Cannot be targeted by Spells, Traps, or card effects. Once per turn, during the End Phase, if this card was Special Summoned: Send it to the GY. You can Tribute 2 monsters; destroy all monsters your opponent controls. This card cannot declare an attack the turn this effect is activated.

10000010 | The Winged Dragon of Ra | 
> Requires 3 Tributes to Normal Summon (cannot be Normal Set). This card's Normal Summon cannot be negated. When Normal Summoned, other cards and effects cannot be activated. The activation(s) and effect(s) of this card cannot be negated. Once per turn, during the End Phase, if this card was Special Summoned: Send it to the GY. When this card is Normal Summoned: This card gains ATK/DEF equal to the combined original ATK/DEF of the Tributes. When this card is Special Summoned: You can pay LP so that you only have 100 left; this card's original ATK and DEF becomes equal to the amount of LP paid. You can pay 1000 LP, then target 1 monster on the field; destroy that target. 

95286165 | De-Fusion | 
> If you control "The Winged Dragon of Ra" this card's activation and effect cannot be negated, also, you can active this card from hand.
● Target 1 Fusion Monster on the field; return that target to the Extra Deck, then, if all the Fusion Materials that were used for its Fusion Summon are in your GY, you can Special Summon all of them.
● Target "The Winged Dragon of Ra" you control, gain LP equal to its original ATK, then make its original ATK and DEF become 0, also, if activated during battle phase then negate the attack of current attacking card.

10000080 | The Winged Dragon of ra - Sphere Mode | #NC
> Can only be Special Summoned while you control no monsters. Cannot be Tributed except for a Tribute Summon. Requires 3 Tributes from either side of the field to Normal Summon to that side of the field (cannot be Normal Set), then shift control to this card's owner during the End Phase of the next turn. Cannot attack. Your opponent cannot target this card for attacks or by card effects. You can Tribute this card; Special Summon 1 "The Winged Dragon of Ra" from your hand or Deck, and if you do, add 1 Spell or Trap that mentions "The Winged Dragon of Ra" in its text from your Deck or GY to your hand; "The Winged Dragon of Ra" cannot activate its effect to be sent to GY by its own effect.

4059313 | Blaze Cannon → God Blaze Cannon | Name only

41044418 | Millennium Revelation | #NC
> "Slifer the Sky Dragon", "The Winged Dragon of Ra", "Obelisk the Tormentor" you control cannot activate their effects during End Phase.
You can only use each effect of "Millennium Revelation" once per turn:
● You can send 1 card from your hand to the GY; add 1 "Monster Reborn" from your Deck or GY to your hand.
● If this card is sent from the field to the GY: You can add 1 card to your Hand that specifically lists any of "Slifer the Sky Dragon", "The Winged Dragon of Ra", "Obelisk the Tormentor" in its name or text from your Deck or GY, except "Millennium Revelation".

39913299 | The True Name | #NC
> Declare 1 card name; excavate the top card of your Deck, and if it is the declared card, add it to your hand, then you can add to your hand, or Special Summon, 1 DIVINE monster from your Deck. Otherwise, send it to the GY. (Quick Effect): You can banish this card from your GY, then target up to 1 card in any GY(s); shuffle it into the Deck. You can only activate 1 "The True Name" per turn.

10000040 | Holactie the Creator of Light | #NC
> Cannot be Normal Summoned/Set. Can only be summoned if you added this card to the hand with the effect of "True Name". Must be Special Summoned (from your hand) by Tributing 3 monsters whose original names are "Slifer the Sky Dragon", "Obelisk the Tormentor", and "The Winged Dragon of Ra", while your Life Points are below 50. This card's Special Summon cannot be negated. The player that Special Summons this card wins the Duel. Once per Duel, you can discard this card to add 1 Divine-Beast monster to your hand from your Deck or GY.

74875003 | Ra's Disciple | 
> When this card is Summoned: You can Special Summon up to 2 other "Ra's Disciple" from your hand, Deck, and/or GY. You can only use this effect of "Ra's Disciple" once per turn. Cannot be Tributed, except for Tribute Summon of "Slifer the Sky Dragon", "Obelisk the Tormentor", or "The Winged Dragon of Ra". Once per turn, you can Tribute Summon "Slifer the Sky Dragon", "Obelisk the Tormentor", or "The Winged Dragon of Ra" from your hand as an additional Normal summon. You cannot Special Summon monsters, except by the effect of "Ra's Disciple". If this card is destroyed by battle; destroy all "Ra's Disciple" you control.

21208154 | The Wicked Avatar | [Divine]
> Must first be Normal summoned. Requires 3 Tributes to Normal Summon or Set. Faceup Divine Fiend Type monsters you control cannot be destroyed by opponent Spell or Trap card effects. The ATK and DEF of this card are each 100 points higher than the highest face-up monster's ATK on the field (except "The Wicked Avatar"). Once per turn, during the End Phase, if this card was Special Summoned: Send it to the GY. This cards' effects cannot be copied.

57793869 | The Wicked Eraser | [Divine]
> Must first be Normal summoned. Requires 3 Tributes to Normal Summon or Set. The ATK and DEF of this card are each equal to the number of cards your opponent controls x 1000. When this card is destroyed and sent to the GY, destroy all cards on the field. Once per turn, during your Main Phase, you can destroy this card. During the End Phase, if this card was Special Summoned: Send it to the GY. This cards' effects cannot be copied.

62180201 | The Wicked Dreadroot | [Divine]
> Must first be Normal summoned. Requires 3 Tributes to Normal Summon or Set. Halve the ATK and DEF of all monsters on the field, except this card. Once per turn, during the End Phase, if this card was Special Summoned: Send it to the GY. This cards' effects cannot be copied.

6007213 | Uria, Lord of Searing Flames | [Divine]
> Cannot be Normal Summoned/Set. Must be First Special Summoned (from your hand) by sending 3 face-up and/or face-down Trap Cards you control to the GY. This card gains 1000 ATK for each continuous Trap Card in your GY and each non-continuous Trap Card with different name in your GY. Once per turn: You can Target 1 Set Spell/Trap Card your opponent controls; destroy that target. Neither player can activate Spell/Trap Cards in response to this effect's activation. You can reveal this card in your hand; Set 1 Trap Card from your hand that specifically lists "Uria, Lord of Searing Flames" in its text, also it can be activated this turn. If this card is in your GY you can discard 1 Trap Card; Special summon it (this effect can only be used once per turn).

32491822 | Hamon, Lord of Striking Thunder | [Divine]
> Cannot be Normal Summoned/Set. Must be First Special Summoned (from your hand) by sending 3 face-up and/or face-down Spell Cards you control to the GY. If this card destroys an opponent's monster by battle and sends it to the GY: Inflict 1000 damage to your opponent. While this card is in face-up Defense Position, monsters your opponent controls cannot target monsters for attacks, except this one. If this card is in your GY you can discard 1 Spell Card; Special summon it in Defense Position, and if you do, you can destroy 1 monster your opponent controls (this effect can only be used once per turn).

69890967 | Raviel, Lord of Phantasms | [Divine]
> Cannot be Normal Summoned/Set. Must be First Special Summoned (from your hand) by Tributing 3 monsters. If your Summons a monster(s): You can Special Summon 1 "Phantasm Token" (Fiend-Type/DARK/Level 1/ATK 1000/DEF 1000) for each monster Summoned, but it cannot declare an attack. Once per turn: You can Tribute 1 monster; this card gains ATK equal to the original ATK of the Tributed monster until the end of this turn. If this card is in your GY you can tribute 2 monsters you control; Special summon it (this effect can only be used once per turn).

30604579 | Thor, Lord of the Aesir | [Divine] | 1 Tuner + 2+ non-Tuner monsters

67098114 | Loki, Lord of the Aesir | [Divine] | 1 Tuner + 2+ non-Tuner monsters 

93483212 | Odin, Father of the Aesir | [Divine] | 1 Tuner + 2+ non-Tuner monsters

25833572 | Gate Guardian |
> Cannot be Normal Summoned/Set. Must first be Special Summoned (from your hand) by Tributing 1 "Sanga of the Thunder", "Kazejin", and "Suijin". This card cannot be destroyed by card effects. (Quick Effect) You can only use 1 of the following effects of "Gate Guardian" per turn, and only once that turn, during damage calculation, if this card attacks or is being attacked by an opponent monster: You can target that monster; 
● switch this card's attack with that target's ATK during damage calculation only. 
● return that target to the hand. 
● destroy that target and special summon 1 level 7 monster from our hand.

25955164 | Sanga of the Thunder |
> Once per turn during damage calculation, if this card attacks an opponent monster: You can target that monster; destroy that target and special summon 1 level 7 monster from our hand. If this card is in your GY: You can pay 1000 LP; add this card to your hand. (This effect of "Sanga of the Thunder" can only be used once per turn.)

62340868 | Kazejin |
> Once per turn during damage calculation, if this card attacks an opponent monster: You can target that monster; return that target to the hand (this is a Quick Effect). This card cannot be targeted by card effects.

98434877 | Suijin |
> Once per turn during damage calculation, if this card attacks an opponent monster: You can target that monster; switch this card's attack with that target's ATK during damage calculation only (this is a Quick Effect). This card cannot be destroyed by battle.

76794549 | Astrograph Sorcerer |
> If a card(s) you control is destroyed by battle or card effect: You can Special Summon this card from your hand, then you can choose 1 Pendulum "Magician" or "Odd-Eyes" or "Supreme King" monster in the GY, Extra Deck, or that is banished, and that was destroyed this turn, and add 1 monster with the same name from your Deck to your hand. You can banish this card you control, plus 4 monsters from your hand, field, and/or GY (1 each with "Pendulum Dragon", "Xyz Dragon", "Synchro Dragon", and "Fusion Dragon" in their names); Special Summon 1 "Supreme King Z-ARC" from your Extra Deck. (This is treated as a Fusion Summon.)

12289247 | Chronograph Sorcerer |
> If a Pendulum "Magician" or "Odd-Eyes" or "Supreme King" you control is destroyed by battle or card effect: You can Special Summon this card from your hand, then you can Special Summon 1 monster from your hand. You can banish this card you control, plus 4 monsters from your hand, field, and/or GY (1 each with "Pendulum Dragon", "Xyz Dragon", "Synchro Dragon", and "Fusion Dragon" in their names); Special Summon 1 "Supreme King Z-ARC" from your Extra Deck. (This is treated as a Fusion Summon.)

13331639 | Supreme King Z-ARC | 
> [ Pendulum Effect ]  
①: Fusion, Synchro, and Xyz Monsters your opponent controls cannot activate their effects.  
②: Once per turn, when your opponent adds a card(s) from their Deck to their hand (except during the Draw Phase or the Damage Step): You can destroy that card(s).  
[ Monster Effect ]    
Must be Fusion Summoned first.  
①: If this card is Fusion Summoned: Destroy all cards your opponent controls and if you do your opponent takes no battle damage from this card's attacks this turn.  
②: Cannot be destroyed by your opponent's card effects.  
③: Your opponent cannot target this card with card effects.  
④: When this card destroys an opponent's monster by battle: You can Special Summon 1 "Supreme King Dragon" monster from your Deck or Extra Deck. If this card in the Monster Zone is destroyed by battle or card effect: You can place this card in your Pendulum Zone.  

22211622 | Supreme King Gate Infinity | 
> ←13 【Pendulum Effect】 13→
①: While you control only "Supreme King" monsters (Quick effect):  
● If you take damage by battle or by a card effect: You can gain LP equal to the damage you took.  
● While you have "Supreme King Gate Zero" in your other scale: You can gain LP equal to the damage you would have taken.  
②: While you control monster(s), you cannot Pendulum Summon except "Supreme king" monsters. This effect cannot be negated.
【Monster Effect】
①: Once per turn: You can target 1 other face-up card you control; destroy both it and this card, and if you do, Special Summon 1 Dragon-Type Xyz or Pendulum Monster from your Extra Deck, but its ATK and DEF become 0, it has its effects negated, also it cannot be used as a Material for a Synchro or Xyz Summon.
②: If this card in the Monster Zone is destroyed by battle or card effect: You can place this card in your Pendulum Zone. 

96227613 | Supreme King Gate Zero | 
> ←0 【Pendulum Effect】 0→
①: If you control only "Supreme King" monsters, you take no damage.
②: Once per turn, if you have "Supreme King Gate Infinity" in your other Pendulum Zone: You can destroy both cards in your Pendulum Zones, and if you do, add 1 "Polymerization" Spell Card or "Fusion" Spell Card from your Deck to your hand.
【Monster Effect】
①: Once per turn: You can target 1 other face-up card you control; destroy both it and this card, and if you do, Special Summon 1 Dragon-Type Fusion or Synchro Monster from your Extra Deck, but its ATK and DEF become 0, it has its effects negated, also it cannot be used as a Material for a Synchro or Xyz Summon.
②: If this card in the Monster Zone is destroyed by battle or card effect: You can place this card in your Pendulum Zone. 

96733134 | Supreme King Dragon Odd-Eyes |
> ←4 【Pendulum Effect】 4→
①: You can Tribute 1 "Supreme King Dragon" monster; destroy this card, and if you do, add 1 Pendulum Monster with 1500 or less ATK from your Deck to your hand.
【Monster Effect】
If your Pendulum Monster battles an opponent's monster, any battle damage it inflicts to your opponent is doubled.
(Quick Effect): You can only use each effect of "Supreme King Dragon Odd-Eyes" once per turn:
● You can Tribute 2 Pendulum Monsters; Special Summon this card in face-up Defense Position from your hand or Extra Deck.
● You can Tribute this card; Special Summon up to 2 face-up "Supreme King Dragon" and/or "Supreme King Gate" Pendulum Monsters from your Extra Deck and/or GY in face-up Defense Position, except "Supreme King Dragon Odd-Eyes".

70771599 | Supreme King Dragon Clear Wing |
> ①: If a Synchro Monster is Synchro Summoned to your opponent's field, you can Special Summon this card (from your Extra Deck) by Tributing 2 faceup "Supreme King" monsters. (This Special Summon is treated as a Synchro Summon.)  
②: If this card is Synchro Summoned: You can negate all face-up monsters your opponent currently controls until End Phase.
③: Once per turn, before damage calculation, if this card battles an opponent's monster: You can destroy that monster, and if you do, inflict damage to your opponent equal to the destroyed monster's original ATK.  
(Quick Effect): You can only use each of following effects of "Supreme King Dragon Clear Wing" per turn: 
● If this card is in your GY: You can Tribute 2 "Supreme King Dragon" monsters; Special Summon this card.  
● You can return this card to the Extra Deck; Special Summon up to 2 "Supreme King Dragon" Pendulum Monsters from your Extra Deck in face-up Defense Position.

43387895 | Supreme King Dragon Starving Venom |
> Can be Special Summoned by Tributing the above cards you control (in which case you do not use "Polymerization").
①: If a Fusion Monster is Fusion Summoned to your opponent's field ,you can immediately Fusion Summon this card (from your Extra Deck) by Tributing 2 "Supreme King" monsters, and if u do the ②nd effect of this card becomes Quick Effect for that turn only.
You can only use each of following effects of "Supreme King Dragon Starving Venom" per turn: 
● You can target 1 other monster on the field or in the GY; until the End Phase, this card's name becomes that monster's original name, and replace this effect with that monster's original effects, also for the rest of this turn, if your monster attacks a Defense Position monster, inflict piercing battle damage to your opponent and your opponent takes no damage from this cards' effects or attacks.
● (Quick Effect): You can return this card to the Extra Deck; Special Summon up to 2 "Supreme King Dragon" Pendulum Monsters from your Extra Deck in face-up Defense Position.

42160203 | Supreme King Dragon Dark Rebellion |
> ①: If an Xyz Monster is Xyz Summoned to your opponent's field, you can Xyz Summon this card ignoring its summoning conditions by using any "Supreme King" monsters as overlay materials and if you do this card becomes unaffected by other card effects until end phase.  
②: Once per turn, before damage calculation, if this card battles an opponent's monster: You can detach 1 material from this card; until the end of this turn, change the ATK of that face-up opponent's monster to 0, and if you do, this card gains ATK equal to the original ATK of that opponent's monster.  
③: (Quick Effect): You can return this card to the Extra Deck; Special Summon up to 2 "Supreme King Dragon" pendulum monsters from your GY in face-up Defense Position. You can only use this effect of "Supreme King Dragon Dark Rebellio" per turn.    

50954680 | Crystal Wing Synchro Dragon | 1 Wind Tuner + 1 non-Tuner "Clear Wing" monster

58074177 | Odd-Eyes Wing Dragon | 1 Tuner + 1 non-Tuner "Clear Wing" monster
> [ Pendulum Effect ]  
①: Once per turn, if a monster you control battles an opponent's monster, before damage calculation: You can make that monster you control gain ATK equal to the current ATK of the opponent's monster it is battling until the end of the Damage Step (even if this card leaves the field).  
[ Monster Effect ]  
(This card is always treated as "Supreme King White Dragon")
If this card in the Monster Zone is destroyed: You can place this card in your Pendulum Zone. You can use each of the following effects of "Odd-Eyes Wing Dragon" once per turn:
①: You can target 1 Effect Monster your opponent controls; it has its effects negated until the end of this turn.
②: During either player's Battle Phase, if this card was Synchro Summoned: You can destroy all Level 5 or higher monsters your opponent controls.

45014450 | Odd-Eyes Venom Dragon |
> [ Pendulum Effect ]
①: Once per turn: You can target 1 Fusion Monster you control; it gains 1000 ATK for each monster your opponent controls, until the end of this turn.  
[ Monster Effect ]  
(This card is always treated as "Supreme King Violet Dragon")
①: Once per turn: You can target 1 face-up monster your opponent controls; until the End Phase, this card gains ATK equal to its ATK, and if it does, this card's name becomes that monster's original name, and replace this effect with that monster's original effects.
②: If this card in the Monster Zone is destroyed: You can Special Summon 1 monster from your Pendulum Zone, and if you do, place this card in your Pendulum Zone.

45627618 | Odd-Eyes Rebellion Dragon |
> [ Pendulum Effect ]  
①: Once per turn, if you have no cards in your other Pendulum Zone: You can place 1 Pendulum Monster from your Deck in your Pendulum Zone.  
[ Monster Effect ]  
(This card is always treated as "Supreme King Black Dragon")
If you can Pendulum Summon Level 7, you can Pendulum Summon this face-up card in your Extra Deck.
①: If this card is Xyz Summoned by using an Xyz Monster as a Material: You can Destroy as many Level 7 or lower monsters your opponent controls as possible, inflict 1000 damage to your opponent for each card destroyed.
②: If this attacking card destroys an opponent's monster by battle: You can detach 1 Xyz Material from this card: It can make a second attack in a row.
③: If this card in the Monster Zone is destroyed by battle or card effect: You can destroy as many cards in your Pendulum Zones as possible (min. 1), and if you do, place this card in your Pendulum Zone.  

86238081 | Odd-Eyes Raging Dragon | 
> [ Pendulum Effect ]  
①: Once per turn, if you have no cards in your other Pendulum Zone: You can place 1 Pendulum Monster from your Deck in your Pendulum Zone.  
[ Monster Effect ]  
(This card is always treated as "Supreme King Blazing Dragon")
If you can Pendulum Summon Level 7, you can Pendulum Summon this face-up card in your Extra Deck.
①: If this card in the Monster Zone is destroyed: You can place it in your Pendulum Zone.
②: If this card is Xyz Summoned using an Xyz Monster as Material, it gains these effects.
● It can make a second attack during each Battle Phase.
● Once per turn: You can detach 1 Xyz Material from this card; destroy as many cards your opponent controls as possible, and if you do, this card gains 200 ATK for each, until the end of this turn.

69610326 | Supreme King Dragon Darkwurm | 
> [ Pendulum Effect ]  
①: Once per turn, if you control no monsters: You can place 1 "Supreme King Gate" Pendulum Monster from your Deck in your Pendulum Zone, also you cannot Pendulum Summon monsters for the rest of this turn, except DARK monsters.  
[ Monster Effect ]  
①: If this card is Normal or Special Summoned: You can add 1 "Supreme King Gate" Pendulum Monster from your Deck to your hand.  
②: If this card is in your GY and you control no monsters: You can Special Summon this card. You can only use each monster effect of "Supreme King Dragon Darkwurm" once per turn.  
③: Your opponent cannot target "Supreme King" cards in your Pendulum Zones with card effects. 

21770839 | Odd-Eyes Phantasma Dragon | 
> When there is a monster in your Pendulum Zone and an "Odd-Eyes" in your Extra Deck, you can Pendulum Summon this card regardless of your Pendulum Scales. If this card attacks an opponent's monster, during damage calculation: You can make that opponent's monster lose 1000 ATK for each face-up Pendulum Monster in your Extra Deck, during that damage calculation only. You can only use this monster effect of "Odd-Eyes Phantasma Dragon" once per turn.

11067666 | White Wing Magician |
> (This card is always treated as a "Synchro Dragon" card.)
If this Pendulum Summoned card is used for a Synchro Summon, banish it. If this card is destroyed by battle or card effect: You can target 1 face-up monster on the field; Negate its effects and if you do Change its Level to 4.

1686814 | Ultimaya Tzolkin |
> (This card's original Level is always treated as 12.)
Cannot be Synchro Summoned. Must be Special Summoned (from your Extra Deck) by sending 2 Level 5 or higher monsters you control with the same Level to the GY (1 Tuner and 1 non-Tuner), and cannot be Special Summoned by other ways. Once per turn, when a Spell/Trap Card(s) is Set on your side of the field (except during the Damage Step): You can Special Summon 1 "Signer Dragon" from your Extra Deck. Cannot be targeted for attacks or by card effects, while you control another Synchro Monster.

```
9012916 | Black-Winged Dragon | 
60992105 | Blackfeather Darkrage Dragon |
```
> (This card is always treated as "Signer Dragon" and "Blackwing" card.)

```
68431965 | Shooting Riser Dragon |
44508094 | Stardust Dragon | 
83994433 | Stardust Spark Dragon |
73580471 | Black Rose Dragon | 
33698022 | Black Rose Moonlight Dragon |
70902743 | Red Dragon Archfiend | 
80666118 | Scarlight Red Dragon Archfiend |
39765958 | Hot Red Dragon Archfiend |
2403771 | Power Tool Dragon | 
25165047 | Life Stream Dragon | 
68084557 | Power Tool Mecha Dragon |
4179255 | Ancient Pixie Dragon |
```
> (This card is always treated as "Signer Dragon" card.)

25862681 | Ancient Fairy Dragon | 
> (This card is always treated as "Signer Dragon" card.)
Once per turn: You can Special Summon 1 Level 4 or lower monster from your hand. You cannot conduct your Battle Phase the turn you activate this effect. Once per turn: You can destroy as many Field Spells on the field as possible, and if you do, gain 1000 LP, then you can add 1 Field Spell with a name different than those that were destroyed from your Deck to your hand. You can only use each effect of "Ancient Fairy Dragon" once per turn.

65330383 | Knightmare Gryphon | 
> If this card is Link Summoned: You can discard 1 card, then target 1 card on the field; return it to the Deck, then, if this card was co-linked when this effect was activated, you can draw 1 card. You can only use this effect of "Knightmare Gryphon" once per turn. Special Summoned monsters on the field except "Knightmare Gryphon" cannot activate their effects except co-linked "Knightmare" monsters.

75452921 | Knightmare Cerberus | 
> If this card is Link Summoned: You can discard 1 card, then target 1 Special Summoned monster in your opponent's Main Monster Zone; destroy it, then, if this card was co-linked when this effect was activated, you can draw 1 card. You can only use this effect of "Knightmare Cerberus" once per turn. Co-linked monsters you control cannot be destroyed by battle.

38342335 | Knightmare Unicorn | 
> If this card is Link Summoned: You can discard 1 card, then target 1 Spell/Trap in your GY; Set it to your field, but it cannot be activated this turn, then, if this card was co-linked when this effect was activated, you can draw 1 card. You can only use this effect of "Knightmare Unicorn" once per turn. While any co-linked "Knightmare" monsters are on the field, for your normal draw in your Draw Phase, draw 1 card for each different card name among those co-linked "Knightmare" monsters, instead of drawing just 1 card.

39064822 | Knightmare Goblin | 
> If this card is Link Summoned during your turn: You can discard 2 card; if this card was co-linked when this effect was activated, you can draw 1 card, also, during your Main Phase this turn, you can Normal Summon 1 monster from your hand to your zone this card points to, in addition to your Normal Summon/Set. You can only apply this effect of "Knightmare Goblin" once per turn. Neither player can target co-linked "Knightmare" monsters you control with card effects.

2857636 | Knightmare Phoenix | 
> If this card is Link Summoned: You can discard 1 card, then target 1 Spell/Trap your opponent controls; destroy it, then, if this card was co-linked when this effect was activated, you can draw 1 card. You can only use this effect of "Knightmare Phoenix" once per turn.
Once per turn, while any co-linked "Knightmare" monsters are on the field pay 1500 LP for each different card name among those co-linked "Knightmare" monsters; Draw 1 card for each 1000 LP lost by this effect.

4168871 | Cards for Black Feathers | 
> Banish 1 "Blackwing" monster from your hand; draw 2 cards. You cannot Special Summon during the turn you activate this card except "Blackwing" monstes. You can only activate 1 "Cards for Black Feathers" per turn.

90809975 | Toadally Awesome |
> During the Standby Phase: You can detach 1 Xyz Material from this card; Special Summon 1 "Frog" monster from your Deck. During either player's turn, when your opponent activates a Spell/Trap Card, or monster effect: You can send 1 Aqua-Type monster from your hand or face-up from your field to the GY; for the rest of the turn "Frog" monsters you control are unaffected by your opponent card's effects until End Phase, this effect can only be used once per turn. If this card is sent to the GY: You can target 1 WATER monster in your GY; add it to your hand.

5043010 | Firewall Dragon | 
> Once while face-up on the field (Quick Effect): You can target monsters on the field and/or GY up to the number of monsters co-linked to this card; return them to the hand, you can only use this effect of "Firewall Dragon" once per turn. If a monster this card points to is destroyed by battle or sent to the GY: You can Send 1 card from your hand to GY; Special Summon 1 monster from your hand.

30539496 | True King Lithosagym, the Disaster |
> If this card is in your hand: You can destroy 2 other monsters in your hand and/or face-up on your field, including an EARTH monster, and if you do, Special Summon this card, and if you do that, and both destroyed monsters were EARTH, you can also look at your opponent's Extra Deck and target up to 2 monsters with different names; cards with those name, and their effects, cannot be used. If this card is destroyed by card effect: You can Special Summon 1 non-EARTH Wyrm-Type monster from your GY. You can only use each effect of "True King Lithosagym, the Disaster" once per turn.

88581108 | True King of All Calamities |
> During either player's turn: You can only use 1 of the following effects of "True King of All Calamities" per turn, and only once that turn; You can detach 1 Xyz Material from this card and declare 1 Attribute and until end phase:  
①: This turn, all face-up monsters on the field become that Attribute.  
②: All monsters in your opponent's possession with that Attribute cannot activate their effects.  
Monsters that "True Draco" and "True King" monsters in your hand would destroy with their effects can be chosen from your opponent's field.

### | Every solemn |  
---
```
92512625   
84749824  
41420027  
40605147
``` 
> →[activation] Until your next turn, any damage your opponent takes becomes halved.

52653092 | Number S0: Utopic ZEXAL | 
> (This card's original Rank is always treated as 1.) You can also Xyz Summon this card by discarding 1 "Rank-Up-Magic" Normal Spell Card, then using a "Utopia" monster you control as the Xyz Material. (Transfer its Xyz Materials to this card.) This card's Xyz Summon cannot be negated. When Xyz Summoned, your opponent's cards and effects cannot be activated and this card's gains ATK and DEF equal to the rank of the material used for its summon x500.
This card gains 500 ATK for each Xyz Material attached to it.
①: Once per turn when your opponent activates a card effect (Quick Effect): You can detach 1 Xyz Materials from this card; this card is unaffected by your opponent's card effects for the rest of this turn. 
②: Once per opponent's turn (Quick Effect): You can detach 2 Xyz Materials from this card; all monsters your opponent controls cannot activate their effect(s) while this card is on the field for the rest of this turn. 


56832966 | Number S39: Utopia the Lightning |
> You can also Xyz Summon this card by using a Rank 4 "Utopia" monster you control as material. (Transfer its Xyz Materials to this card.) Cannot be used as material for an Xyz Summon. If this card battles, your opponent cannot activate cards or effects until the end of the Damage Step. Once per Chain, during damage calculation, if this card battles an opponent's monster while this card has a "Utopia" Xyz Monster as material (Quick Effect): You can detach 2 materials from this card; this card's ATK becomes 5000 during that damage calculation only. (Cannot declare an attack while you control other monsters except "Number" or Token monsters.)

83531441 | Dante, Traveler of the Burning Abyss | 2 Level 3 Fiend type monsters

74586817 | PSY-Framelord Omega | 1 PSY-Frame Tuner + 1+ non-Tuner Psychic type monsters

30100551 | Minerva, the Exalted Lightsworn | 2 Level 4 "Lightsworn" monsters

29587993 | Mist Valley Apex Avian |
> [+] Cannot be pendulum summoned.

21076084 | Trickstar Reincarnation |
> Shuffle your opponent's entire hand into their deck, and if you do, they draw the same number of cards. You can banish this card from your GY, then target 1 "Trickstar" monster in your GY; Special Summon it.

52687916 | Trishula, Dragon of the Ice Barrier |
> 1 Tuner + 2+ non-Tuner monsters
When this card is Synchro Summoned: You can banish up to 1 card each from your opponent's hand, field, and GY. (The card in the hand is chosen at random.) You can only use this effect of "Trishula, Dragon of the Ice Barrier" once per turn.

44665365 | Herald of Perfection |
> You can Ritual Summon this card with "Dawn of the Herald". During either player's turn, when your opponent activates a Spell Card, Trap Card, or monster effect: You can send the same type of card(Spell Card, Trap Card, or monster) from your hand to the GY; negate the activation.

38179121 | Double Evolution Pill | 
> Banish 1 Dinosaur monster and 1 non-Dinosaur monster from your hand and/or Field; Special Summon 1 Level 7 or higher Dinosaur monster from your hand or Deck, ignoring its Summoning conditions. You can only activate 1 "Double Evolution Pill" per turn.

18940556 | Ultimate conductor Tyranno | 
> Cannot be Normal Summoned/Set. Must first be Special Summoned (from your hand) by banishing 2 Dinosaur-Type monsters from your GY. Once per turn, during either player's Main Phase: You can destroy 1 monster in your hand or field, and if you do, change all face-up monsters your opponent controls to face-down Defense Position. This card can attack all monsters your opponent controls, once each. 

24224830 | Called by the Grave |
> Each player target 1 monster in their opponent's GY; banish those targets, and if you do, until the end of the next turn, their effects are negated, as well as the activated effects and effects on the field of monsters with the same original name.

82732705 | Skill Drain |
> Activate by paying 1000 Life Points. Declare a number from 1 to 12; The effects of all face-up monsters with the same Level/Rank/Link Rating as the selected value on the field are negated while those monsters are face-up on the field (but their effects can still be activated).

53804307 | Blaster, Dragon Ruler of Infernos |
> If this card is in your hand or GY: You can banish a total of 2 FIRE and/or Dragon-Type monsters from your hand and/or GY, except this card; Special Summon this card. During your opponent's End Phase, if this card was Special Summoned: Return it to the hand. You can discard this card and 1 FIRE monster to the GY, then target 1 card on the field; destroy that target. If this card is banished: You can add 1 FIRE Dragon-Type monster from your Deck to your hand. You can only use 1 "Blaster, Dragon Ruler of Infernos" effect per turn, and only once that turn.
Cannot be used as a Material for a Tribute, Synchro, Xyz Summon, Fusion or Link Summon except for a Dragon-Type monster.

26400609 | Tidal, Dragon Ruler of Waterfalls |
> If this card is in your hand or GY: You can banish a total of 2 WATER and/or Dragon-Type monsters from your hand and/or GY, except this card; Special Summon this card. During your opponent's End Phase, if this card was Special Summoned: Return it to the hand. You can discard this card and 1 WATER monster to the GY; send 1 "Dragon Ruler" monster from your Deck to the GY. If this card is banished: You can add 1 WATER Dragon-Type monster from your Deck to your hand. You can only use 1 "Tidal, Dragon Ruler of Waterfalls" effect per turn, and only once that turn.
Cannot be used as a Material for a Tribute, Synchro, Xyz Summon, Fusion or Link Summon except for a Dragon-Type monster.

90411554 | Redox, Dragon Ruler of Boulders |
> If this card is in your hand or GY: You can banish a total of 2 EARTH and/or Dragon-Type monsters from your hand and/or GY, except this card; Special Summon this card. During your opponent's End Phase, if this card was Special Summoned: Return it to the hand. You can discard this card and 1 EARTH monster to the GY, then target 1 "Dragon Ruler" monster in your GY; Special Summon that target. If this card is banished: You can add 1 EARTH Dragon-Type monster from your Deck to your hand. You can only use 1 "Redox, Dragon Ruler of Boulders" effect per turn, and only once that turn.
Cannot be used as a Material for a Tribute, Synchro, Xyz Summon, Fusion or Link Summon except for a Dragon-Type monster.

89399912 | Tempest, Dragon Ruler of Storms |
> If this card is in your hand or GY: You can banish a total of 2 WIND and/or Dragon-Type monsters from your hand and/or GY, except this card; Special Summon this card. During your opponent's End Phase, if this card was Special Summoned: Return it to the hand. You can discard this card and 1 WIND monster to the GY; add 1 "Dragon Ruler" monster from your Deck to your hand. If this card is banished: You can add 1 WIND Dragon-Type monster from your Deck to your hand. You can only use 1 "Tempest, Dragon Ruler of Storms" effect per turn, and only once that turn.
Cannot be used as a Material for a Tribute, Synchro, Xyz Summon, Fusion or Link Summon except for a Dragon-Type monster.

15291624 | Thunder Dragon Colossus |
> Must be either Fusion Summoned, or Special Summoned during the turn a Thunder monster's effect was activated in the hand, by Tributing 1 Thunder Effect non-Fusion Monster (in which case you do not use "Polymerization"). Once per turn: You can target 1 of your "Thunder Dragon" monsters that is banished or is in GY add that target to the hand. If this card would be destroyed by battle or card effect, you can banish 1 Thunder monster from your GY instead.
------------------
24508238 | D.D. Crow |
> (Quick Effect): During your main phase: you can discard this card to the GY, then target 1 card in your opponent's GY; banish that target.

15397015 | Inspector Boarder |
> Cannot be Normal or Special Summoned if you control a monster. Neither player can activate monster effects.

72634965 | Vanity's Ruler |
> This card cannot be Special Summoned. While this card is the only monster you control, your opponent cannot Special Summon monsters.

47084486 | Vanity's Fiend |
> Cannot be Special Summoned. While this card is the only monster you control, neither player can Special Summon monsters.

58481572 | Masked HERO Dark Law |
> Must be Special Summoned by "Mask Change". Any card sent to your opponent's GY is banished instead. Once per turn, if your opponent adds a card(s) from their Deck to their hand (except during the Draw Phase or the Damage Step): You can banish 1 random card from both side of the fields.

10045474 | Infinite Impermanence |
> Discarding 1 other card except "Infinite Impermanence" to activate. If you control no cards, you can activate this card from your hand. Target 1 face-up monster your opponent controls; negate its effects (until the end of this turn), then, if this card was Set before activation and is on the field at resolution, for the rest of this turn all other Spell/Trap effects in this column are negated.

14558127 | Ash Blossom & Joyous Spring |
> When a card or effect is activated that includes any of these effects (Quick Effect): You can discard this card and send 1 other card from your hand to GY; negate that effect.
● Add a card from the Deck to the hand.  
● Special Summon from the Deck.   
● Send a card from the Deck to the GY.  
You can only use this effect of "Ash Blossom & Joyous Spring" once per turn.

59438930 | Ghost Ogre & Snow Rabbit |
> When a monster on the field activates its effect, or when a Spell/Trap that is already face-up on the field activates its effect (Quick Effect): You can send 2 cards from your hand or field including this card to the GY; destroy that card on the field. You can only use this effect of "Ghost Ogre & Snow Rabbit" once per turn.

62015408 | Ghost Reaper & Winter Cherries |
> If your opponent controls more monsters than you do (Quick Effect): You can discard this card; reveal 1 card in your Extra Deck, then look at your opponent's Extra Deck, also banish all cards in both Extra Decks with the same name as that revealed card. You can only use this effect of "Ghost Reaper & Winter Cherries" once per turn.

73642296 | Ghost Belle & Haunted Mansion |
> When a card or effect is activated that includes any of these effects (Quick Effect):  You can discard this card and send 1 other card from your hand to GY; negate that activation.  
● Add a card(s) from the GY to the hand, Deck, and/or Extra Deck.  
● Special Summon a Monster Card(s) from the GY.  
● Banish a card(s) from the GY.  
You can only use this effect of "Ghost Belle & Haunted Mansion" once per turn.

63845230 | Eater of Millions |
> Cannot be Normal Summoned/Set. Must be Special Summoned (from your hand) by banishing 5 or more cards from your hand, field, and/or Extra Deck, face-down. This card gains 200 ATK/DEF for each face-down banished card. This card cannot be Tributed, nor used as material for a Fusion, Synchro, or Xyz Summon.

55063751 | Gameciel, the Sea Turtle Kaiju |
> You can Special Summon this card (from your hand) to your opponent's field in Attack Position, by Tributing 1 monster they control. If your opponent controls a "Kaiju" monster, you can Special Summon this card (from your hand) in Attack Position. You can only control 1 "Kaiju" monster. When your opponent activates a Spell or Trap card effect (Quick Effect): You can remove 2 Kaiju Counters from anywhere on the field; negate the activation, and if you do, banish that card.

61665245 | Summon Sorceress |
> If this card is Link Summoned: You can Special Summon 1 Dragon or Spellcaster from your hand in Defense Position, to your opponent's zone this card points to. You can target 1 face-up monster this card points to; Special Summon 1 Dragon or Spellcaster type monster from your Deck with the same Type as that monster from your Deck in Defense Position, with the same Type as that monster, to a zone this card points to, but negate its effects. You can only use this effect of "Summon Sorceress" once per turn.

59537380 | Agarpain the Guardragon |
> You can only Special Summon "Agarpain the Guardragon(s)" once per turn. During your Main Phase: You can Special Summon 1 Dragon Link monster from your Extra Deck to the Extra Monster Zone or your zone, 2 or more Link Monsters point to, also, you cannot Special Summon monsters for the rest of this turn except "World Chalice", "World Legacy" and/or Dragon Link monsters. You can only use this effect of "Agarpain the Guardragon" once per turn.

86148577 | Guardragon Elpy |
> During your Main Phase: You can Special Summon 1 Dragon monster from your hand or Deck to your zone that 2 or more Link Monsters point to, also, you cannot Special Summon monsters for the rest of this turn except "World Chalice", "World Legacy" and/or Dragon Link monsters. You can only use this effect of "Guardragon Elpy" once per turn. You can only Special Summon "Guardragon Elpy(s)" once per turn.

13143275 | Guardragon Pisty |
> During your Main Phase: You can target 1 of your Dragon monsters that is banished or in your GY; Special Summon it to your zone that 2 or more Link Monsters point to, also, you cannot Special Summon monsters for the rest of this turn except "World Chalice", "World Legacy" and/or Dragon Link monsters. You can only use this effect of "Guardragon Pisty" once per turn. You can only Special Summon "Guardragon Pisty(s)" once per turn.

59934749 | Isolde, Two Tales of the Noble Knights | 2 "Noble Knights" monsters

24094258 | Heavymetalfoes Electrumite |
> If this card is Link Summoned: You can add 1 Pendulum Monster from your Deck to your Extra Deck face-up. Once per turn: You can target 1 other face-up "Metalfoes" card you control; destroy it, then add 1 face-up Pendulum Monster from your Extra Deck to your hand. If a card(s) in your Pendulum Zone leaves the field: Draw 1 card. You can only use this effect of "Heavymetalfoes Electrumite" once per turn.

50588353 | Crystron Halqifibrax | 2 "Crystron" monsters, including a Tuner

55623480 | Fairy Tail - Snow |
> If this card is Normal or Special Summoned: You can target 1 face-up monster your opponent controls; change it to face-down Defense Position. If this card is in your GY (Quick Effect): You can banish 7 other cards from your hand or field; Special Summon this card.

52352005 | XX-Saber Gottoms |
> You can Tribute 2 "X-Saber" monster to discard 1 random card from your opponent's hand.

58069384 | Cyber Dragon Nova | 2 Level 5 Machine monsters, including 1 "Cyber Dragon" monster

33198837 | Naturia Beast | 1 "Naturia" Tuner + 1 or more non-Tuner "Naturia" monsters 

2956282 | Naturia Barkion |  1 "Naturia" Tuner + 1 or more non-Tuner "Naturia" monsters 

23440231 | Infernoid Devyaty |
> Cannot be Normal Summoned/Set. Must be Special Summoned (from your hand or GY) by banishing 3 "Infernoid" monsters from your hand or GY while the total Levels and Ranks of all Effect Monsters you control are 8 or lower. When this card is Special Summoned: You can destroy all Spells and Traps on the field, except "Void" cards. Once per turn, when another monster's effect is activated (Quick Effect): You can Tribute 2 monsters except "Infernoid Devyaty"; negate the activation, and if you do, banish that card.

14799437 | Infernoid Onuncu |
> Cannot be Normal Summoned/Set. Must be Special Summoned (from your hand or GY) by banishing 3 "Infernoid" monsters from your hand or GY while the total Levels and Ranks of all Effect Monsters you control are 8 or lower, and cannot be Special Summoned by other ways. When this card is Special Summoned: You can destroy all other monsters on the field. Once per turn, during either player's turn, when a Spell/Trap Card or effect is activated: You can Tribute 2 monsters except "Infernoid Onuncu"; negate the activation, and if you do, banish that card.

### All Danger
---

```
16209941 | Danger! Chupacabra! | 
26302107 | Danger! Dogman! | 
43316238 | Danger! Bigfoot! | 
43694650 | Danger!? Jackalope? | 
52350806 | Danger! Mothman! | 
70711847 | Danger! Nessie! | 
90807199 | Danger! Thunderbird! | 
99745551 | Danger!? Tsuchinoko? | 
83518674 | Danger! Ogopogo! |
```  
>Then, if the discarded card was not:  
①: "X", Special Summon 1 "X" from your hand, and if you do, draw 1 card.  
②: a "Danger!" card; Send 1 random card from your hand to GY.  

4423206 | M-X-Saber Invoker | 2 Level 3 "X-Saber" monsters

5008836 | Exodia, Master of The Guard |
> Cannot be Special Summoned. You can Tribute 5 monsters to Tribute Summon (but not Set) this card. This card's ATK/DEF becomes the combined original ATK/DEF of the Tributed monsters; each "Forbidden One" is considered 1000 ATK/DEF. If this card is Summoned by tributing 5 different "Forbidden One" cards destroys a Dark monster that is owned by your opponent by battle, you win the Duel.  

18326736 | Tellarknight Ptolemaeus | 2 Level 4 monsters

91279700 | Evilswarm Ophion  | 
> Must first be Xyz Summoned with the above Xyz Materials. While this card has Xyz Material, Level 5 or higher monsters cannot be Special Summoned. Once per turn: You can detach 1 Xyz Material from this card; add 1 "Infestation" Spell/Trap Card from your Deck to your hand.

93854893 | Sheorcust Dingirsu | 
> You can only Special Summon "Dingirsu, the Orcust of the Evening Star(s)" once per turn. You can also Xyz Summon this card by using an "Orcust" Link Monster you control as material. If a card(s) you control would be destroyed by battle or card effect, you can detach 1 material from this card instead. If this card is Special Summoned: You can activate 1 of these effects;
●Detach 1 material from this card; send 1 card your opponent controls to the GY.
●Attach 1 of your banished Machine monsters to this card as material.

34945480 | Outer Entity Azathot |  
> You can also Xyz Summon this card by using an "Outer Entity" Xyz Monster you control as material. (Transfer its materials to this card.) Cannot be used as material for an Xyz Summon. If this card was Xyz Summoned during your turn, monster effects cannot be activated for the rest of that turn except "Outer Entity". If this card has at least 3 Xyz Materials (1 Fusion, 1 Synchro, and 1 Xyz Monster): You can detach 1 material from this card; destroy all cards your opponent controls.

15939229 | D/D/D Duo-Dawn King Kali Yuga  |   
> Must first be Xyz Summoned with the above Xyz Materials. After this card is Xyz Summoned, for the rest of this turn, other cards and their effects cannot be activated on the field, and other cards' effects on the field are negated. Once per turn, during either player's turn: You can detach 1 Xyz Material from this card; destroy all Spell and Trap Cards on the field. You can detach 1 Xyz Material from this card, then target 1 "Dark Contract" Spell/Trap Card in your GY; Set that target.

72402069 | D/D/D Super Doom King Bright Armageddon | 
> Your opponent cannot target monsters you control with card effects. If another monster is Normal or Special Summoned while this monster is on the field (except during the Damage Step): You can make your opponent choose 1 Pendulum Monster they control, and all other monsters they currently control have their effects negated, until the next time a monster(s) is Normal or Special Summoned. If this card in the Monster Zone is destroyed: You can place this card in your Pendulum Zone.

26692769 | The Phantom Knights of Rusty Bardiche | 2+ DARK monsters, including at least 1 "Phantom Knights" monster

58820923 | Number 95: Galaxy-Eyes Dark Matter Dragon | 
> You can also Xyz Summon this card by using a "Galaxy-Eyes" Xyz Monster you control as the Xyz Material. (Xyz Materials attached to that monster also become Xyz Materials on this card.); Cannot be used as an Xyz Material for an Xyz Summon. When this card is Xyz Summoned: You can send 3 "Photon" and/or "Galaxy" monsters with different names from your Deck to the GY; your opponent banishes 3 monsters from their Deck. You can detach 1 Xyz Material from this card; this card can make up to 2 attacks on monsters during each Battle Phase this turn.

2390019 | Ojamassimilation | 
> Reveal 1 LIGHT Machine Fusion Monster from your Extra Deck and banish any number of "Ojama" monsters from your hand and/or face-up field; Special Summon Fusion Materials with different names whose names are specifically listed on the card you revealed, from your hand, Deck, and/or GY, equal to the number of monsters banished to activate this effect. You can banish this card from your GY, then target 3 of your banished "Ojama" monsters; shuffle them into the Deck, then draw 1 card. 

96561011 | Red-Eyes Darkness Dragon | 
> Cannot be Normal Summoned/Set. Must first be Special Summoned (from your hand) by Tributing 1 "Red-Eyes B. Dragon". This card gains 300 ATK for each Dragon monster in your GY. When a Spell or Trap card is activated that targets this card (Quick Effect): You can negate that effect. (Quick Effect): You can Tribute this card: Special Summon "Red-Eyes Darkness Metal Dragon" from your hand or GY. 

88264978 | Red-Eyes Darkness Metal Dragon | 
> You can Special Summon this card (from your hand) by banishing 1 face-up Dragon-Type monster you control. 
If this card was summoned by the effect of "Red-Eyes Darkness Dragon" the ②nd effect of this card becomes quick effect.  
①: This card gains 200 ATK for each "Red-Eyes" in your GY.  
②: Once per turn: You can Special Summon 1 Dragon-Type monster from your hand or GY; cannot be used as a Material for a Synchro or Link Summon; this effect can only be used once per turn.

53347303 | Blue-Eyes Shining Dragon | 
> Cannot be Normal Summoned/Set. Must first be Special Summoned (from your hand) by Tributing 1 "Blue-Eyes" Fusion monster. The activation(s) and effect(s) of this card cannot be negated. Gains 300 ATK for each Dragon monster in your GY. When a card or effect is activated that targets this card (Quick Effect): You can negate that effect. (Quick Effect): You can Tribute this card; Send 1 card from the field to the GY (this is treated as being destroyed). You can banish 1 face-up "Blue-Eyes" monster you control; Special summon this card from GY.

8240199 | Sage with Eyes of Blue |
> When this card is Summoned: You can add 1 Level 1 LIGHT Tuner from your Deck to your hand, except "Sage with Eyes of Blue". You can discard this card, then target 1 Effect Monster you control; send it to the GY, and if you do, Special Summon 1 "Blue-Eyes" monster from your Deck. You can only use each effect of "Sage with Eyes of Blue" once per turn.

45644898 | Master with Eyes of Blue |
> When this card is Summoned: You can target 1 Level 1 LIGHT Tuner monster in your GY; add it to your hand. You can shuffle this card from your GY into the Deck, then target 1 Effect Monster you control; send it to the GY, and if you do, Special Summon 1 "Blue-Eyes" monster from your GY, other than the sent monster. You can only use each effect of "Master with Eyes of Blue" once per turn.

72855441 | Protector with Eyes of Blue |
> When this card is Summoned: You can Special Summon 1 Level 1 LIGHT Tuner monster from your hand. You can target 1 Effect Monster you control; send it to the GY, and if you do, Special Summon 1 "Blue-Eyes" monster from your hand. You can only each effect of "Protector with Eyes of Blue" once per turn.

24382602 | Mausoleum of White |
> During your Main Phase, you can Normal Summon 1 Level 1 LIGHT Tuner in addition to your Normal Summon/Set. (You can only gain this effect once per turn.) Once per turn: You can target 1 face-up monster you control; Shuffle 1 Normal Monster from your hand or GY into the Deck, and if you do, the targeted monster gains ATK/DEF equal to the Level of the monster shuffled into the Deck x 100, until the end of this turn (even if this card leaves the field). You can banish this card from your GY; add 1 "Burst Stream of Destruction" from your Deck to your hand.

45467446 | Dragon Spirit of White |
> (This card is always treated as a "Blue-Eyes" card.)
This card is treated as a Normal Monster while in the hand or GY. When this card is Normal or Special Summoned: You can target 1 Spell/Trap your opponent controls; banish it. If your opponent controls a monster (Quick Effect): You can Tribute this card; Special Summon 1 "Blue-Eyes White Dragon" from your GY. You can only this effect of "Dragon Spirit of White" once per turn.

5126490 | Neos Wiseman |
> Cannot be Normal Summoned or Set. Must First be Special Summoned (from your hand) by sending 1 face-up "Elemental HERO Neos" and 1 face-up "Yubel" you control to the GY. This card cannot be destroyed by card effects. At the end of the Damage Step, if this card battled an opponent's monster: Inflict damage to your opponent equal to the ATK of the monster it battled, and you gain Life Points equal to that monster's DEF. When this card is destroyed by battle, you can remove from play 1 "Yubel" from your GY to Special Summon 1 "Elemental HERO Neos" from your GY.

51339637 | Salamangreat Roar |
> Activate 1 of these effects.
● Send 1 "Salamangreat" monster from your hand or face-up field to the GY, then target 1 card on the field; destroy it.  
● Target 1 "Salamangreat" Link Monster you control that was Link Summoned using a monster with its same name as material, tribute it; destroy cards your opponent controls, up to that monster's Link Rating.
You can only activate 1 "Salamangreat Rage" per turn.

27869883 | Shadowpriestess of Ohm |
> You can pay 1400 Life points and Tribute 1 face-up DARK monster you control to inflict 800 damage to your opponent.

84664085 | Satellite Warrior | 2 Tuner + 1+ non-Tuner Synchro Monsters |  
> If this card is Synchro Summoned: You can target cards your opponent controls, up to the number of Synchro Monsters in your GY; destroy them, and if you do, this card gains 1000 ATK for each card destroyed. If this Synchro Summoned card is destroyed: You can Special Summon up to 3 Level 8 or lower "Warrior", "Synchron", and/or "Stardust" Synchro Monsters with different names from your GY. You can only use 1 of "Satellite Warrior" effects per turn, and only once that turn.

58270977 | Magistry Alchemist |
> Normal Trap  
Banish 4 "HERO" monsters from your GY and/or face-up field, then target 1 "HERO" monster in your GY; Special Summon it, ignoring its Summoning conditions, and if EARTH, WATER, FIRE, and WIND Attributes are all among the monsters you banished to activate this effect, double the Summoned monster's original ATK, and if you do that, negate the effects of all face-up cards your opponent currently controls and until end phase only the monster special summoned by this card's effect can declare an attack. You can only activate 1 "Magistry Alchemist" per turn.

11481610  | Performapal Pop-Up  
> Send up to 3 cards from your hand to the GY including at least 1 "Performapal", "Magician" Pendulum Monsters, and/or "Odd-Eyes"; draw the same number of cards you sent to the GY, then, if you have 2 cards in your Pendulum Zones, you can Special Summon "Performapal" monsters, "Magician" Pendulum Monsters, and/or "Odd-Eyes" monsters from your hand with different names that have Levels between (exclusive) the Pendulum Scales of the cards in your Pendulum Zones, up the number of cards you drew by this effect.  
If you did not Special Summon by this card's effect, you lose 1000 LP for each card in your hand.  
You can only activate 1 "Performapal Pop-Up" per turn.  

37818794 | Red-Eyes Dark Dragoon |
> Neither player can target or destroy this card with card effects. During your Main Phase: You can destroy 1 monster your opponent controls, and if you do, inflict damage to your opponent equal to that monster's original ATK. This effect can be used a number of times per turn, up to the number of Normal Monsters used as Fusion Material for this card's Summon. Once per turn, when a card or effect is activated (Quick Effect): You can discard 1 cards; destroy that card, and if you do, this card gains 1000 ATK.

```
10963799 | Barrier Statue of the Torrent |
19740112 | Barrier Statue of the Drought |
46145256 | Barrier Statue of the Heavens |
47961808 | Barrier Statue of the Inferno |
73356503 | Barrier Statue of the Stormwinds |
84478195 | Barrier Statue of the Abyss |
```
> +Cannot be special summoned.

10669138 | Link God Dragon |  
> Must be Link Summoned. If this card is Link Summoned using DARK, EARTH, WATER, FIRE, and WIND monsters as material: You can destroy all cards your opponent controls. Unaffected by other cards' effects, also cannot be destroyed by battle with a DARK, EARTH, WATER, FIRE, or WIND monster. Once, during each End Phase: You can banish 5 cards from your GY, face-down. If you don't, send this card to the GY.

90953320 | T.G. Hyper Librarian |
> If a monster is Synchro Summoned: Draw 1 card. You cannot Special Summon monsters from the Extra Deck the turn you activate this effect, except Synchro Monsters. This card must be face-up on the field to activate and to resolve this effect.

71525232 | Gandora-X the Dragon of Demolition |
> When this card is Normal or Special Summoned from the hand: You can destroy as many other monsters on the field as possible, and if you do, inflict damage to your opponent equal to the highest original ATK among those destroyed face-up monsters (your choice, if tied). This card's ATK becomes equal to the damage inflicted to your opponent by this effect. Once per turn, during your End Phase: Halve your LP.

21200905 | Aromaseraphy Jasmine | 2 Plant monsters, including at least 1 "Aroma" monster

97631303 | Magicians' Souls |
> You can send up to 2 Spells/Traps from your hand and/or field to the GY; draw that many cards. If this card is in your hand: You can send 1 "Palladium", "Dark Magician" or "Dark Magician Girl" monster or 1  monster that specifically lists any of those cards in its text from your Deck to the GY, then activate 1 of these effects;
● Special Summon this card.
● Send this card to the GY, then, you can Special Summon 1 "Dark Magician" or 1 "Dark Magician Girl" from your GY.
You can only use each effect of "Magicians' Souls" once per turn.

28570310 | Mythical Beast Garuda |
> Each time a Spell Card is activated, place 1 Spell Counter on this card when that Spell resolves. When your opponent Normal or Special Summons a monster(s) (except during the Damage Step): You can remove 6 Spell Counters from your field; Special Summon this card from your hand, then return the opponent's Summoned monster(s) to the hand. You can only use this effect of "Mythical Beast Garuda" once per turn.

3040496 | Chaos Ruler the Chaotic Demonic Dragon | 1 LIGHT Tuner + 1+ DARK non-Tuner monsters

20899496 | Ice Dragon's Prison |  
> Target 1 monster in your opponent's GY; Special Summon it to your field, but its effects are negated, then, you can banish 1 monster from both players' fields that have the same Type as each other except the summoned monster. You can only activate 1 "Ice Dragon's Prison" per turn.

94977269 | El Shaddoll Winda |
> Must first be Fusion Summoned. Cannot be destroyed by an opponent's card effects. While you control only "Shaddoll" monsters, each player can only Special Summon monster(s) once per turn while this card is face-up on the field. If this card is sent to the GY: You can target 1 "Shaddoll" Spell/Trap in your GY; add it to your hand.

70369116 | Predaplant Verte Anaconda | 
> You cannot Special Summon monsters, except "Predaplant" or "Fusion Dragon".
You can target 1 face-up monster on the field; it becomes DARK until the end of this turn. You can pay 2000 LP and send 1 "Fusion" or "Polymerization" Normal or Quick-Play Spell from your Deck to the GY; this effect becomes that Spell's effect when that card is activated. You can only use each effect of "Predaplant Verte Anaconda" once per turn.

46772449 | Evilswarm Exciton Knight | 2 Level 4 "lswarm" monsters

44097050 | Mecha Phantom Beast Auroradon | 2+ "Phantom Beast" monsters

4837861 | Meklord Astro Dragon Triskelia |
> Cannot be Normal Summoned/Set. Must first be Special Summoned (from your hand) by banishing 3 "Meklord" monsters with different names from your GY. Once per turn, when this card declares an attack: You can equip 1 monster from either GY. This card gains ATK equal to the ATK of the monster equipped to it by its effect. This card equipped with a Synchro Monster can make up to 3 attacks on monsters during each Battle Phase.

85243784 | Linkross |
> If this card is Link Summoned: You can activate this effect; Special Summon a number of "Link Tokens" (Cyberse/LIGHT/Level as the Link Rating of the monster used for this card's Link Summon/ATK 0/DEF 0) up to the Link Rating of the monster used for this card's Link Summon, also you cannot use "Link Tokens" as Link Material for the rest of this turn. You can only use this effect of "Linkross" once per turn.

95679145 | Dogmatika Maximus |
> You can banish 1 Fusion, Synchro, Xyz, or Link Monster from your GY; Special Summon this card from your hand. During your Main Phase: You can send 2 monsters with different names from your Field to the GY, also your opponent sends 2 monsters from their Field to the GY, also you cannot Special Summon from the Extra Deck for the rest of this turn. You can only use each effect of "Dogmatika Maximus" once per turn.

22073844 | Dogmatika Nexus |
> Cannot be Special Summoned while you control a monster(s) other than "Dogmatika". Cannot be Normal Summoned/Set. Must be Special Summoned by its own effect. You can target 4 Fusion, Synchro, Xyz, and/or Link Monsters in the GYs; Special Summon this card from your hand, and if you do, banish those targets. At the start of the Damage Step, if this card battles a Special Summoned monster: Destroy as many opponent's Attack Position monsters as possible, then inflict 800 damage to your opponent for each Fusion, Synchro, Xyz, or Link Monster destroyed. Cannot special summon monsters except "Dogmatika".

1984618 | Nadir Servant |
> Send 1 "Dogmatika" monster from your Deck to the GY, then add 1 "Dogmatika" monster or "Fallen of Albaz" from your Deck or GY to your hand, that has ATK less than or equal to that sent monster in the GY, also, for the rest of this turn after this card resolves, you cannot Special Summon monsters from the Extra Deck. You can only activate 1 "Nadir Servant" per turn.

82956214 | Dogmatika Punishment |
> Target 1 face-up monster your opponent controls; send 1 monster from your Field to the GY, and if you do, destroy the targeted monster. Until the end of your next turn after this card resolves, you cannot Special Summon from the Extra Deck. You can only activate 1 "Dogmatika Punishment" per turn.

16317140 | Hyper Blaze |
> Once per turn, you can reveal 1 "Uria, Lord of Searing Flames" from your hand; set 2 Trap cards with the same name from your Deck or GY they cannot be activated and cannot be returned to hand. Once per battle, when an attack is declared involving your "Uria, Lord of Searing Flames": You can send 1 Trap from your hand or Deck to the GY; its ATK/DEF become the number of face-up Traps on the field and in the GYs x 1000, for the rest of this turn. Once per turn: You can discard 1 card; add to your hand or Special Summon, 1 "Uria, Lord of Searing Flames", "Hamon, Lord of Striking Thunder", or "Raviel, Lord of Phantasms" from your GY, ignoring its Summoning conditions.

54828837 | Cerulean Skyfire |
> Once per turn, you can reveal 1 "Hamon, Lord of Striking Thunder" from your hand; set 2 Spell cards with the same name from your Deck or GY they cannot be activated and cannot be returned to hand. Once per turn, while you control an Attack Position "Hamon, Lord of Striking Thunder", you can negate any Spell/Trap effect activated by your opponent, then, change 1 "Hamon, Lord of Striking Thunder" you control to Defense Position. If a face-up "Uria, Lord of Searing Flames", "Hamon, Lord of Striking Thunder", or "Raviel, Lord of Phantasms" you control leaves the field: You take no damage this turn.

28651380 | Raviel, Lord of Phantasms - Shimmering Scraper |
> Cannot be Normal Summoned/Set. Must be Special Summoned (from your hand) by discarding 2 other cards. You can only use each of the following effects of "Raviel, Lord of Phantasms - Shimmering Scraper" once per turn.
●(Quick Effect): You can tribute this card Special Summon 3 "Phantasmal Martyr Tokens" (Fiend-Type/DARK/Level 1/ATK 0/DEF 0) in Attack Position.
●(Quick Effect): You can discard this card and target 1 "Raviel, Lord of Phantasms" you control; for the rest of this turn, its ATK becomes double its current ATK, also it can attack all monsters your opponent controls once each; for the rest of the turn any damage your opponent takes becomes halved.
●If this card is in your GY: You can Tribute 1 monster; add this card to your hand.

# 4. Unknown/TBD Errata
---
## 4.1. Beta 
---

## 4.1.1 Scripted
---
92519087 | Datascape Fox - Xianxian |  
*prev*  
> Any card sent from the field to the GY is banished instead.

*new*  
> If you control only "Datascape" mosnters, any card sent from the field to the GY is banished instead.  


87481592 | Dogmatikatism
*prev*  
> If this card is sent to the GY because the equipped monster is 

*new*  
> If this card is sent to the GY because the equipped monster is destroyed by an opponent's attack or card effect:

83190280 | Lunalight Tiger |
> Once per turn: You can target 1 "Lunalight" monster in your GY; Special Summon it, but it cannot attack, its effects are negated, also it is destroyed during the End Phase. You can only use this effect of "Lunalight Tiger" once per turn.

47021196 | U.A. Playing Manager | 
> If you Normal or Special Summon an "U.A." monster(s) (except during the Damage Step): You can Special Summon this card from your hand. If this card is Special Summoned: You can activate 1 of these effects.
● Target 1 card on the field; destroy it.
● Negate the effects of all face-up monsters currently on the field and you cannot special summon monsters until the end of this turn, except "U.A." monsters';
You can only use each effect of "U.A. Playing Manager" once per turn.

64276752 | Arc Rebellion Xyz Dragon |
> This Xyz Summoned card cannot be destroyed by card effects. You can detach 1 material from this card and if this card has a:
● "Rebellion" monster as material; this card gains ATK equal to the combined half of original ATK of all other monsters on the field.
● "Xyz Dragon" monster as material; negate the effects of all other face-up monsters on the field.
After this effect resolves, you cannot declare attacks with other monsters for the rest of this turn. You can only each effect of "Arc Rebellion Xyz Dragon" once per turn.

90448279 | Divine Arsenal AA-ZEUS - Sky Thunder |
> Once per turn, if an Xyz Monster battled this turn, you can also Xyz Summon "Divine Arsenal AA-ZEUS - Sky Thunder" by using 2 Xyz Monsters you control as material. (Quick Effect): Once per turn, you can detach 3 materials from this card; send all other cards on the field to the GY. Once per turn, if another card(s) you control is destroyed by battle or an opponent's card effect: You can attach 1 card from your hand, Deck, or Extra Deck to this card as material.

28781003 | Raiders' Knight | 2 Level 4 "The Phantom Knights" and/or "Raidraptor" monsters

66750703 | Fire Fortress atop Liang Peak |
> 10: Special Summon 1 Beast-Warrior monster from your Deck or Extra Deck. i̶g̶n̶o̶r̶i̶n̶g̶ ̶i̶t̶s̶ ̶S̶u̶m̶m̶o̶n̶i̶n̶g̶ ̶c̶o̶n̶d̶i̶t̶i̶o̶n̶s̶.̶

17016131 | Pride of the Plunder Patroll |
> If your "Plunder Patroll" monster destroys an opponent's monster by battle: Draw 1 card. If you control a "Plunder Patroll" monster that was normal summoned: You can send this face-up card from your field to the GY, then activate 1 of these effects;
● Your opponent draws 1 card, then you look at their hand and send 1 random card from it to the GY.
● Look at your opponent's Extra Deck and target 1 monster from it; until your next turn, your opponent cannot summon monsters with that card's name.
You can only use each effect of "Pride of the Plunder Patroll" once per turn.

53184342 | Dragunity Arma Gram |  
*prev*  
> You can banish 2 Dragon and/or Winged Beast monsters from your GY; Special Summon this card from your hand or GY. You can target 1 face-up monster on the field; negate its effects, and if you do, it loses 1000 ATK for each Equip Card you control. When an opponent's monster is destroyed by battle and sent to the GY: You can equip it to this card as an Equip Spell. You can only use each effect of "Dragunity Arma Gram" once per turn.

*new*
> While you don't control monsters other than "Dragunity"; You can banish 2 Dragon and/or Winged Beast monsters from your GY; Special Summon this card from your hand or GY, for the rest of the turn you cannot special summon monster except "Dragunity". You can target 1 face-up monster on the field; negate its effects, and if you do, it loses 1000 ATK for each Equip Card you control. When an opponent's monster is destroyed by battle and sent to the GY: You can equip it to this card as an Equip Spell. You can only use each effect of "Dragunity Arma Gram" once per turn.

34522216 | Amorphage Gluttony |
> If this card is Pendulum Summoned or flipped face-up, while you control only "Amorphage" monsters, while this card is face-up on the field, neither player can Special Summon monsters from the Deck, except "Amorphage" monsters.

70917315 | Amorphage Lechery |
> If this card is Pendulum Summoned or flipped face-up, while you control only "Amorphage" monsters, while this card is face-up on the field, neither player can Destroy cards by card effects, except for "Amorphage" monsters.

7305060 | Amorphage Greed |
> If this card is Pendulum Summoned or flipped face-up, while you control only "Amorphage" monsters, while this card is face-up on the field, neither player can Special Summon monsters from the Hand, except "Amorphage" monsters.

33300669 | Amorphage Envy |
> If this card is Pendulum Summoned or flipped face-up, while you control only "Amorphage" monsters, while this card is face-up on the field, neither player can Special Summon monsters from the GY, except "Amorphage" monsters.

79794767 | Amorphage Wrath |
> If this card is Pendulum Summoned or flipped face-up, while you control only "Amorphage" monsters, while this card is face-up on the field, neither player can Tribute monsters, except "Amorphage" monsters.

6283472 | Amorphage Pride |
> If this card is Pendulum Summoned or flipped face-up, while you control only "Amorphage" monsters, while this card is face-up on the field, neither player can Target cards, except "Amorphage" monsters.

32687071 | Amorphage Sloth |
> While you control only "Amorphage" monsters, neither player can Special Summon monsters from the Extra Deck, except "Amorphage" monsters.

69072185 | Amorphage Goliath |
> Once per turn, target 1 face-up card on your side of the field; consider it as "Amorphage". While you control only "Amorphage" monsters, neither player can Special Summon monsters from the Extra Deck, except "Amorphage" monsters.

55285840 | Time Thief Redoer | 2 Level 4 monsters, including at least 1 "Time Thief" monster

37164373 | Madolche Queen Tiaramisu | 
> Once per turn: You can detach 1 Xyz Material from this card, then target up to 2 "Madolche" cards in your GY; shuffle those cards into the Deck, also, after that, shuffle cards your opponent controls into the Deck, up to the number of "Madolche" cards returned. You cannot Special Summon monsters the turn you activate this card, except "Madolche" monsters.

97489701 | Red Nova Dragon | 2 Tuners + "Red Dragon Archfiend"
> This card gains 500 ATK for each Tuner monster in your GY. Cannot be destroyed by an opponent's card effects. When a monster declares an attack: You can target the attacking monster; banish this card, and if you do, negate that attack. Once per turn, during the next End Phase after this card was banished: Special Summon this banished card.

99585850 | Red Supernova Dragon | 3 Tuners + 1+ non-Tuner "Archfiend" and/or "Signer Dragon" Synchro Monsters
> Must first be Synchro Summoned. Gains 500 ATK for each Tuner in your GY. Cannot be destroyed by your opponent's card effects. Once per turn when a monster declares an attack (Quick Effect): You can banish all cards on the field. Once per turn, during your next End Phase after this card was banished: Special Summon this banished card.

26268488 | Stardust Sifr Divine Dragon | 
> Must be Synchro Summoned and cannot be Special Summoned by other ways. Once per turn, when a card or effect is activated that targets this card (Quick Effect): You can negate the effect, and if you do, destroy 1 card on field. The first time each card you control would be destroyed each turn, by battle, it is not destroyed, and if it would be by a card effect, you can destroy 1 other card on the field instead.
You can banish this card from your GY, then target 1 Level 8 or lower "Stardust" monster in your GY; Special Summon it.

21123811 | Cosmic Blazar Dragon |
> Must be Synchro Summoned. (Quick Effect): You can banish this card until the End Phase to activate 1 of these effects;
●When your opponent activates a card or effect: Negate the activation.
●When your opponent would Summon a monster(s): Negate the Summon, and if you do, destroy that monster(s).
●When an opponent's monster declares an attack: Negate the attack, and if you do, banish all face-up attack position monsters on field until the End Phase.

35952884 | Shooting Quasar Dragon |
> Must be Synchro Summoned and cannot be Special Summoned by other ways. You take no effect damage. When a card or effect is activated that includes any of these effects (Quick Effect):
You can negate that effect, and if you do, destroy that card.
● Negate an effect or a card.
● Target this card.
You can only use each of the following effects of this effect of "Shooting Quasar Dragon" once per turn.
You can banish this card from your GY, then target 1 Level 8 or lower "Stardust" monster in your GY; Special Summon it.

62242678 | Hot Red Dragon Archfiend King Calamity |
> If this card destroys a monster by battle: Inflict damage to your opponent equal to that monster's original ATK. Monsters' destruction by the battle cannot be negated. If this card in its owner's possession is destroyed by an opponent's card: Negate the effects of all monsters on field then you can target 1 Level 8 or lower DARK Dragon Synchro Monster in your GY; Special Summon it.

98127546 | Underworld Goddess of the Closed World |
> You can also use 1 monster your opponent controls as material to Link Summon this card. If this card is Link Summoned: You can negate the effects of all face-up monsters your opponent currently controls. This Link Summoned card is unaffected by your opponent's activated effects, unless they target this card. Once per turn, when your opponent activates a card or effect that Special Summons a monster(s) from the GY (Quick Effect): You can negate the activation. You cannot Special Summon monsters, except Link monsters.

21438286 | Archfiend Staff of Despair |
> Equip only to a monster you control. During your Main Phase: You can make all monsters on the field except the equiped monster lose ATK equal to half the equipped monster's ATK, until the end of this turn (even if this card leaves the field). If this card is sent to the GY while equipped to a monster: You can pay 1000 LP; return this card to your hand. You can only use each effect of "Archfiend's Staff of Despair" once per turn.

2602411 | Wizard Buster Destruction Sword |
> You can target 1 "Buster Blader" you control; equip this monster from your hand or your side of the field to that target. While this card is equipped to a "Buster Blader" or "Destruction Sword" monster, monsters in your opponent's GY cannot activate their effects. You can send this Equip Card to the GY, then target 1 "Destruction Sword" monster in your GY, except "Wizard Buster Destruction Sword"; add it to your hand.

76218313 | Dragon Buster Destruction Sword |
>You can target 1 "Buster Blader" you control; equip this monster from your hand or field to that target.  While this card is equipped to a "Buster Blader" or "Destruction Sword" monster, your opponent cannot Special Summon monsters from the Extra Deck. While this card is equipped to a monster: You can Special Summon this equipped card. You can only use this effect of "Dragon Buster Destruction Sword" once per turn.

38601126 | Robot Buster Destruction Sword |
> You can target 1 "Buster Blader" you control; equip this monster from your hand or your side of the field to that target. While this card is equipped to a "Buster Blader" or "Destruction Sword" monster, Spell/Trap Cards that are already face-up on your opponent's side of the field cannot activate their effects. You can send this Equip Card to the GY; the monster that was equipped with this card gains 1000 ATK until the end of this turn.

58092907 | Performapal Celestial Magician |
> During your Main Phase, if this card was Normal or Special Summoned this turn: You can apply the following effect(s) for the rest of this turn, based on the other monsters you currently control.
● Fusion: This card can attack directly.
● Synchro: This card becomes unaffected by your opponent's monster effects.
● Xyz: This card's ATK becomes double its original ATK.
● Pendulum: During the End Phase, add 1 Pendulum Monster from your Deck to your hand.
You can only use this effect of "Performapal Celestial Magician" once per turn.

58153103 | Armed Dragon Thunder LV10 |
> If this card was Special Summoned by the effect of an "Armed Dragon" monster, it gains effects based on its own ATK.
● 1000+: This card's name becomes "Armed Dragon LV10".
● 1500+: Possession of this card cannot switch.
● 2000+: Cannot be destroyed by battle.
● 2500+: Once per turn, during your opponent's turn (Quick Effect): You can send 1 card from your hand to the GY, then target 1 other card on the field; destroy it, and if you do, this card gains 1000 ATK.
● 10000+: Once per turn: You can destroy all other cards on the field.

61606250 | Armed Dragon Blitz |
> Target 1 "Armed Dragon" monster you control; take 1 monster with the same name from your Deck or GY, and either add it to your hand or Special Summon it,  but it cannot attack directly. You can only activate 1 "Armed Dragon Blitz" per turn. You cannot Special Summon monsters the turn you activate this card, except Dragon monsters.

34293667 | Ice Barrier |
> When an opponent's monster declares an attack: Target the attacking monster; Change that opponent's monster's ATK to 0, its battle position cannot be changed, also negate its effects. You can banish this card from your GY; you cannot Special Summon monsters until the end of your next turn, except WATER monsters, also send 1 Level 5 or higher WATER monster from your Deck to the GY, then you can add 1 WATER monster from your GY to your hand. You can only use this effect of "Ice Barrier" once per turn.

70980824 | Trishula, Zero Dragon of the Ice Barrier |
> When this card is Synchro Summoned: You can banish up to 3 cards your opponent controls, after that banish the equal amount of cards on your side of the field. If this Synchro Summoned card in its owner's control is destroyed by an opponent's card: You can Special Summon 1 "Trishula, Dragon of the Ice Barrier" from your Extra Deck or GY, its ATK becomes 3300, then halve the ATK of any face-up monsters your opponent currently controls, also negate their effects until end phase. You can only use this effect of "Trishula, Zero Dragon of the Ice Barrier" once per turn.

6075533 | Terror of Trishula |
> Apply one of these effects, based on the number of "Ice Barrier" Synchro Monsters with different names you control.
● 1+: Banish 1 card your opponent controls.
● 2+: Banish 1 card from your opponent's GY.
● 3+: Banish 1 random card from your opponent's hand.
When your opponent activates a card or effect that targets an "Ice Barrier" Synchro Monster(s) you control: You can banish this card from your GY; negate that effect. You can only use each effect of "Pulse of Trishula" once per turn.

53265336 |  Doremichord Scale |
Apply any of these effects, in sequence, based on the number of "Doremichord" Pendulum Monster Cards with different names you control.
● 3+: Return 1 card from your Pendulum Zone to the hand, and if you do, place 1 face-up "Doremichord" Pendulum Monster from your Extra Deck in your Pendulum Zone.
● 5+: Special Summon 1 "Doremichord" Pendulum Monster from your hand.
● 6+: Destroy all face-up cards in Monster Zone.
You can only activate 1 "Doremichord Scale" per turn.

89707961 | Pharaonic Guardian Sphinx |
> Once per turn: You can change this card to face-down Defense Position. You can only use each of the following effects of "Pharaonic Guardian Sphinx" once per turn. If this card is Flip Summoned: Shuffle all monsters on the field except this card into the Deck. If this card in its owner's control is destroyed by an opponent's card: You can Special Summon 1 Level 5 Rock monster from your hand or Deck in face-down Defense Position.

48608796 | Lyrilusc - Assembled Nightingale |
> This card's attack becomes equal to 150 ATK for each Xyz Material attached to it. This card can attack directly. While this card has Xyz Material, it can attack a number of times each Battle Phase, up to the number of Xyz Materials attached to it. Once per turn, during either player's turn: You can detach 1 Xyz Material from this card; until the end of this turn, "Lyrilusc" monsters you control cannot be destroyed by battle or card effects, also you take no battle damage.

8243121 | Lyrilusc - Birds of a Feather |
> Target 1 "Lyrilusc" monster you control; until end phase the ATK of all face-up monsters become the targeted monster's ATK, also their Levels/Ranks become 1. When an opponent's monster declares an attack on your "Lyrilusc" monster: You can banish this card from your GY; your battling monster's ATK becomes that opponent's monster's ATK, until the end of this turn. You can only use each effect of "Lyrilusc - Birds of a Feather" once per turn.

56619778 | Lyrilusc - Bird Strike |
> If you control a "Lyrilusc" monster, discard 2 cards: Negate the effects of all face-up monsters your opponent currently controls until the end of this turn. You can only activate 1 "Lyrilusc - Bird Strike" per turn.

19489718 | Magikey Blaster - Batosbuster |
> You can Ritual Summon this card with "Magikey - Maphteah". If this card is Ritual Summoned: You can add 1 "Magikey" card from your Deck to your hand. You can only use this effect of "Magikey Blaster - Batosbuster" once per turn. Once per turn, when an attack is declared involving this card and an opponent's monster with the same Attribute as a Normal Monster or "Magikey" monster in your GY: You can place any number of cards from your hand on the bottom of the Deck, and if you do, negate the effects of that opponent's monster (until the end of this turn).

62849088 | Sword Master of the Bewitching Iris |
> During the Main Phase, if a monster whose effects are negated is on the field (Quick Effect): You can Special Summon this card from your hand. If your opponent Special Summons a monster(s) (except during the Damage Step): You can activate 1 of these effects, depending on where that monster(s) was Special Summoned from;
● Hand: Special Summon 1 monster from your hand.
● Deck: Shuffle 2 cards from your hand into the the deck; Draw 2 cards.
● Extra Deck: Destroy 1 of those monsters Special Summoned from the Extra Deck.
You can only use each effect of "Sword Master of the Bewitching Iris" once per turn.

84339249 | Protector Spirit Loagaeth |
> If this card on the field is destroyed by battle or card effect: You can target 1 monster on the field; it cannot be destroyed by battle this turn. You can only use each of the following effects of "Protector Spirit Loagaeth" once per turn. If a Fairy monster you control activates its effect (except during the Damage Step): You can Special Summon this card from your hand. You can target 1 face-up card your opponent controls and 1 Attack Position monster you control; banish both cards.

58844135 | Antagonistic Intelligence ME-PSY-YA |
> You can reveal this card in your hand; add 1 other Pendulum Monster from your hand or 1 card from your Pendulum Zone to the Extra Deck face-up, and if you do, Special Summon this card. You can only use this effect of "Antagonistic Intelligence ME-PSY-YA" once per turn. If another monster(s) is Normal or Special Summoned while this monster is on the field target that monster(s): Send that target(s) to the GY during the End Phase of this turn.

2609443 | Chronomaly Vimana | 2 Level 5 monsters including at least 1 "Chronomaly" monster

3322931 | Wolfrayet the Stellar Wind Wolf |
> You can only control 1 "Wolfrayet the Stellar Wind Wolf". While this card has less than 4000 ATK, each time another monster's effect is activated, add to this card original ATK 300 ATK immediately after it resolves. Once per turn, if this card has 4000 or more original ATK (Quick Effect): You can shuffle this card and all monsters your opponent controls into the Deck.

32768230 | Eternal Cyber |
> Target 1 Machine "Cyber" Fusion Monster in your GY; either return it to the Extra Deck or Special Summon it, ignoring its Summoning conditions, by shuffling into the Deck, from your hand, field, or GY, the Fusion Materials that are listed on it (This is treated as a Fusion Summon). If a Machine "Cyber" Fusion Monster(s) you control would be destroyed by battle or card effect, you can banish this card from your GY instead. You can only use each effect of "Eternal Cyber" once per turn.

6309986 | Five Star Twilight |
> If the only monster you control is 1 Level 5 monster: Tribute that monster; Special Summon 1 "Kuribah", 1 "Kuribee", 1 "Kuriboo", 1 "Kuribeh", and 1 "Kuriboh" from your hand, Deck, and/or GY, but they cannot be used as materials for Extra Deck monsters. You can only activate 1 "Five Star Twilight" per turn.

32245230 | Contract of Destiny |
Each time a card(s) you control is destroyed by battle or card effect, place 1 Emperor's Key Counter on this card (max. 1). While you control no monster(s); if your opponent Special Summons a monster(s) from the Extra Deck: You can remove 1 Emperor's Key Counter from this card; send 1 "The Door of Destiny" from your hand, Deck, or face-up field to the GY, and if you do, Special Summon 1 LIGHT "Utopia" or "Utopic" Xyz Monster from your Extra Deck, and if you do that, attach this card to it as material. (This is treated as an Xyz Summon.) You can only use this effect of "Contract of Destiny" once per turn.

64591429 | Astral Kuriboh |
You can reveal 1 "Number" Xyz Monster in your Extra Deck; Special Summon this card from your hand, and if you do, its Level becomes equal to the revealed monster's Rank, also you cannot Special Summon monsters from the Extra Deck, except "Number" Xyz Monsters, while this card is face-up in the Monster Zone. You can only use this effect of "Astral Kuriboh" once per turn. A "Number" monster that was Xyz Summoned using this card on the field as material gains this effect.
● This card cannot be destroyed by battle except by a "Number" monster.

---
## NOT added im cdb
68300121 | Magikey-Possessed Spirit - Weparthu | 2 Level 4 "Magikey" monsters 

41215808 | Flundereeze and the City of Dreams |
> During the Main Phase: Immediately after this effect resolves, Normal Summon 1 Level 4 or lower Winged Beast monster. If you Tribute Summon a Level 7 or higher "Flundereeze" monster while this card is in your GY: You can banish this card; target a monsters your opponent controls change its position to face-down Defense Position. You can only use this effect of "Flundereeze and the City of Dreams" once per turn.

55521751 | Flundereeze and the Unknown Wind | 
> You can conduct a Tribute Summon that requires 2 Tributes by sending 1 monster you control to the GY instead of Tributing (it is still treated as a Tribute Summon). During your Main Phase: You can reveal up to 2 Winged Beast monsters in your hand and place them on the bottom of the Deck in any order, then draw the same number of cards. You can only use this effect of "Flundereeze and the Unknown Wind" once per turn.

53212882 | Flundereeze x Snowl |
> Once per turn, if you control this Tribute Summoned card: You can activate this effect; you can conduct 3 Normal Summons/Sets this turn, not just 1. While you control this Tribute Summoned card, if your monster attacks a Defense Position monster, inflict piercing battle damage to your opponent. Once per opponent’s turn (Quick Effect): You can banish 1 card from your hand; change 1 of opponent’s Special Summoned monsters to face-down Defense Position.

80611581 | Flundereeze x Empen |
> If this card is Tribute Summoned: You can add 1 "Flundereeze" Spell/Trap from your Deck to your hand, then, immediately after this effect resolves, you can Normal Summon 1 monster. Once per battle, if this card battles an opponent's monster, during damage calculation (Quick Effect): You can banish 1 card from your hand; that opponent's monster's current ATK/DEF become halved, until the end of this turn.

60461804 | Destiny HERO – Destroy Phoenix Enforcer |
1 Level 6 or higher "HERO" Fusion monster + 1 "Destiny HERO" monster
> Monsters your opponent controls lose 200 ATK for each "Destiny HERO" card in your GY. You can only use each of the following effects of "Destiny HERO - Destroy Phoenix Enforcer" once per turn. (Quick Effect): You can destroy both 1 card you control and 1 card on the field. If this card is destroyed by battle or card effect: You can activate this effect; Special Summon 1 "Destiny HERO" monster from your GY during the Standby Phase of the next turn.

15943341 | Reloaded Cylinder |
> Set 1 "Magic Cylinder" directly from your Hand or GY, and if it was Set from the GY, it can be activated this turn. When you activate "Magic Cylinder": You can banish this card from your GY; double the damage inflicted to your opponent by its effect. You can only use this effect of "Reloaded Cylinder" once per turn.

27548199 | Borreload Savage Dragon | 1 Tuner "Rokket" monster + 1+ non-Tuner monsters | 1 Tuner monster + 1+ non-Tuner "Borrel" monsters

68987122 | Borrelguard Dragon | 3+ Effect Monsters including 2 "Rokket" or 1 "Borrel" monsters

31833038 | Borreload Dragon | 3+ Effect Monsters including 2 "Rokket" or 1 "Borrel" monsters

85289965 | Borrelsword Dragon | 3+ Effect Monsters including 2 "Rokket" or 1 "Borrel" monsters

98630720 | Borrelend Dragon | 3+ Effect Monsters including 2 "Rokket" and/or 1 "Borrel" monsters
> Must first be link summoned.

67288539 | Borrelcode Dragon | 2+ Effect Monsters including 2 "Rokket" or 1 "Borrel" monsters

9822220 | King of the Sky Prison |
> During your Main Phase: You can reveal this card in your hand (until you activate a Spell/Trap card), and if you do, while this card is revealed by this effect, Set cards on the field cannot be destroyed by card effects. If a Set Spell/Trap Card is activated: You can Special Summon this card from your hand, then, if you activated this effect while this card was revealed in the hand, you can Set 1 Spell/Trap directly from your Hand, but banish it during the End Phase of the next turn. You can only use 1 "King of the Sky Prison" effect per turn, and only once that turn.

1784686 | The Eye of Timaeus |
> (This card is also always treated as "Legendary Dragon Timaeus" Dragon/Effect/LIGHT/Level 8/ATK 2800/DEF 1800.)
You can only activate 1 effect of "The Eye of Timaeus" per turn. Cannot be Summoned/Set, except by its own effect.
[Spell/Monster Effect]
Fusion Summon 1 Fusion Monster using 2 monsters, including 1 listed material, from your hand or field as Fusion Material, you can also use this card as Fusion Material.
[Monster Effect]
You can Special Summon this card from your hand by sending 1 "Dark Magician" you control to GY. 

46232525 | The Claw of Hermos |
>(This card is also always treated as "Legendary Dragon Hermos" Dragon/Effect/LIGHT/Level 8/ATK 2800/DEF 1800.)
You can only activate 1 effect of "The Claw of Hermos" per turn. Cannot be Summoned/Set, except by its own effect.
[Spell/Monster Effect]
Send 1 monster from your hand or field to the GY (if that card is Set, reveal it):
● Target 1 face-up monster on the field; equip this card to that target. Once per turn: it can active 1 of original effects of the sent monster in the GY.
● Send this card to GY; Special Summon a Fusion monster from your Extra Deck that can only be Special Summoned with "The Claw of Hermos" that lists the type of the monster sent to GY.
[Monster Effect]
You can Special Summon this card from your GY by sending 1 "Red-Eyes Black Dragon" you control to GY. 

11082056 | The Fang of Critias |
>(This card is also always treated as "Legendary Dragon Critias" Dragon/Effect/LIGHT/Level 8/ATK 2800/DEF 1800.)
You can only activate 1 effect of "The Fang of Critias" per turn. Cannot be Summoned/Set, except by its own effect.
[Spell Effect]
Send 1 Normal Trap card from your hand or field to the GY; Special Summon this card; 
[Monster Effect]
Send 1 Normal Trap card from your hand or field to the GY:
● Send this card to GY; Special Summon a Fusion monster from your Extra Deck that can only be Special Summoned with "The Fang of Critias" that lists the the card sent to GY. 
(Quick effect) Once per turn: You can activate 1 effect of the trap(s) that was sent to GY by this card.
You can Special Summon this card from your hand by shuffling 1 "Blue-Eyes White Dragon" from your GY into the Deck.

90036274 | Clear Wing Fast Dragon | 1 Tuner + 1+ non-Tuner monsters
> ←4 【Pendulum Effect】 4→
You can send 1 face-up Tuner and 1 face-up non-Tuner monster you control to the GY, whose total Levels equal 7; Special Summon this card from your Pendulum Zone. You can only use this effect of "Clear Wing Fast Dragon" once per turn.
【Monster Effect】
1 Tuner + 1+ non-Tuner monsters
(Quick Effect): You can target 1 face-up monster your opponent controls that was Special Summoned from the Extra Deck; until the end of this turn, change its ATK to 0, also negate that face-up monster's effects. You can only use this effect of "Clear Wing Fast Dragon" once per turn. If this card in the Monster Zone is destroyed by battle or card effect: You can place this card in your Pendulum Zone.

## 4.1.2. Not yet Scripted
---

DIFO-JP077 ダイノルフィア・フレンジー Dinorphia Frenzy
Normal Trap Card
You can only activate a card with this card’s name once per turn.
(1) During your opponent’s Main Phase: Pay half of your LP; Fusion Summon 1 “Dinorphia” Fusion Monster from your Extra Deck using 1 monster from your Deck and 1 monster you control.
(2) When your opponent activates a card or effect while your LP are 2000 or less: You can banish this card from your GY; you take no damage from your opponent’s card effects this turn.

DIFO-JP038 ダイノルフィア・レクスターム Dinorphia Rexsturm
Level 8 DARK Dinosaur Fusion Effect Monster
ATK 3000
DEF 0
Materials: 1 “Dinorphia” Fusion Monster + 1 “Dinorphia” Monster
You can only use the (2) and (3) effects of this card’s name once per turn each.
(1) Monsters on field with an ATK equal or higher than your LP cannot activate their effects.
(2) (Quick Effect): You can pay half of your LP; until the end of this turn, the ATK of all monsters your opponent controls becomes equal to your LP.
(3) If this card is destroyed in battle or by a card effect: You can Special Summon 1 Level 6 or lower “Dinorphia” monster from your GY.

78135071 | Exorsister Caspitelle |
> Cannot be destroyed by battle with a monster Special Summoned from the GY. You can only use each of the following effects of "Exorsister Caspitelle" once per turn. If this card is Xyz Summoned by using an "Exorsister" monster as material: If didn't Special Summon monsters from the GY; for the rest of the turn, neither player can Special Summon monsters from the GY. You can detach 1 material from this card; add 1 "Exorsister" monster from your Deck to your hand.

41524885 | Exorsister Usophiel |
> Cannot be destroyed by the activated effects of monsters Special Summoned from the GY. You can only use each of the following effects of "Exorsister Usophiel" once per turn. If this card is Xyz Summoned by using an "Exorsister" monster as material: If didn't activate card effects in the GY; this turn, neither player can activate card effects in the GY. You can detach 1 material from this card, then target 1 monster your opponent controls; return it to the hand.

5530780 | Exorsister Jibrine |
> Cannot be destroyed by the activated effects of monsters Special Summoned from the GY. You can only use each of the following effects of "Exorsister Jibrine" once per turn. If this card was Xyz Summoned this turn by using an "Exorsister" monster as material (Quick Effect): You can target 1 Effect Monster your opponent controls which was summoned from GY; negate its effects until the end of this turn. You can detach 1 material from this card; for the rest of this turn, all Xyz Monsters you control gain 800 ATK.

30802207 | Exorsister Carpedibel |
> Neither player can target "Exorsister" monsters you control with effects of monsters Special Summoned from the GY. You can only use each of the following effects of "Exorsister Carpedibel" once per turn. If you Xyz Summon an "Exorsister" monster: You can declare 1 card name; for the rest of this turn, negate the activated effects and effects on the field of cards with that original name (even if this card leaves the field). When an attack is declared involving your "Exorsister" monster: You can target 1 Spell/Trap your opponent controls; destroy it.

85059922 | Dark Master of Chaos |
> If this card is Fusion Summoned: You can target 1 LIGHT or DARK monster in your GY; Special Summon it. You can Tribute 1 LIGHT monster; banish all cards in this card's column. If this card is banished you can Tribute 1 DARK monster; special summon it. If this Fusion Summoned card is destroyed by battle or card effect: You can target 1 Spell in your GY; add it to your hand. You can only use each effect of "Dark Master of Chaos" once per turn.

64797746 | Nordic Relic Svalinn |
> "Aesir" monsters cannot be destroyed by card effects. You can Tribute 1 "Nordic" monster, then target 1 "Aesir" monster in your GY; Special Summon it. You can only use 1 "Nordic Relic Svalinn" effect per turn, and only once that turn.

73104892 | Irohamomiji |
1 Tuner + 2+ non-Tuner non-Token monsters
If this card is Synchro Summoned: You can declare 1 Attribute; this card gains this effect.
● All face-up monsters on the field become the declared Attribute.
You can target 1 monster in your opponent's Main Monster Zone; your opponent must send 1 card in its adjacent Monster Zone or Spell & Trap Zone to the GY. You can only use each effect of "Irohamomiji" once per turn.

100284002 | Life Shaver |
> You can only control 1 "Life Shaver". Once per turn, during your opponent's End Phase: Place 1 counter on this card. During the Main or Battle Phase, if this card has a counter: You can send this card to the GY, then you and your opponent discard as many cards as possible from the hand, up to the number of counters that were on this card.

30576089 | Blue-Eyes Jet Dragon |
> Other cards you control cannot be destroyed by your opponent's card effects. You can only use each of the following effects of "Blue-Eyes Jet Dragon" once per turn, and can only activate them while "Blue-Eyes White Dragon" is on your field or in your GY. If a card(s) on the field is destroyed by battle or card effect: You can Special Summon this card from your GY (if it was there when the card was destroyed) or hand (even if not). At the start of the Damage Step, if this card battles: You can target 1 card your opponent controls; return it to the hand.

HC01-JP046 Unmei no Shuujin (Prisoners of Fate / Prisoners of Destiny)
Continuous Trap
You can only activate 1 card with this card’s name per turn.
(1) Once per Chain, if a Link-4 monster is Link Summoned: You can place 1 counter on this card (max. 3), then you can apply one of these effects, depending on the number of those counters on this card.
● 1: Declare 1 card name, and for the rest of the turn, negate the activated effects and effects on the field of cards with that original name.
● 2: Special Summon 1 Level 4 or lower monster from your GY.
● 3: Send this card to the GY, and if you do, Special Summon 1 Link-4 monster from your Extra Deck.

HC01-JP035 Kahyoureigetsu ((The) Wonders of Nature/Graceful Song under a Frosty Moon)
Equip Spell Card
You can only use the 1st and 2nd effect of this card’s name each once per turn.
(1) Special Summon 1 Level 4 or lower Fairy, Spellcaster, Winged Beast, or Beast-Warrior monster from your hand, and if you do, equip it with this card, then you can return all Dragon monsters on the field to the hand.
(2) During the End Phase, if this card is in your GY because it was sent there while it was face-up in the Spell & Trap Zone this turn; You can add 1 “Polymerization” Spell, 1 “Fusion” Spell, or 1 “Fusion Parasite” from your Deck to your hand.

## 4.2. Unknown
---

44097050 | Mecha Phantom Beast Auroradon | 2+ Machine monsters
> If this card is Link Summoned: You can activate this effect; Special Summon 3 "Mecha Phantom Beast Tokens" (Machine/WIND/Level 3/ATK 0/DEF 0), also you cannot Link Summon for the rest of this turn. Once per turn: You can Tribute up to 3 monsters, then apply 1 of these effects, based on the number Tributed;
●1: Destroy 1 card on the field.
●2: Special Summon 1 "Mecha Phantom Beast" monster from your Deck.
●3: Add 1 Trap from your GY to your hand.

Book of Lunar Eclipse
Quick-Play Spell
(1) Discard 1 card, then target 2 face-up monsters on the field; change them to face-down Defense Position.

33212663 | Arknemesis Eschatos |
> Cannot be Normal Summoned/Set. Must first be Special Summoned (from your hand) by banishing 3 monsters with different Types from your GY and/or face-up field. Cannot be destroyed by card effects. You can declare 1 Monster Type on the field; destroy all monsters on the field with that Type, also neither player can Special Summon monsters with that Type until the end of the next turn and you cannot activate cards or effects until the end of your next turn. You can only use this effect of "Arknemesis Eskatos" once per turn.

6728559 | Archnemesis Protos |
> Cannot be Normal Summoned/Set. Must first be Special Summoned (from your hand) by banishing 3 face-up monsters with different Attributes you control and/or in your GY. Cannot be destroyed by card effects. You can declare 1 Attribute on the field; destroy all monsters on the field of that Attribute, also neither player can Special Summon monsters of that Attribute until the end of the next turn and you cannot activate cards or effects until the end of your next turn. You can only use this effect of "Archnemesis Protos" once per turn.

87188910 | Ravenous Crocosaur Archetis | 1 WATER Tuner + 1+ non-Tuner WATER monsters

### 4.2.1. On Watch
---

San Seed Shadow (Sunseed Shadow) - (summons from extra deck)  
Lavalval Salamander - (easy to summon and generic effects)  
Fairy Knight Ingunar - (mass return without counter)  

### 4.2.2. Banlist
---

---
### To do
```
Remove/Replace unnecessary card text:
- `while this card is face-up on the field` - ?
- `(Quick Effect)` - `(QE)`
- `Once per turn` - `OPT`
Prototype:
- `You can only use each effect of "X" once per turn`										 - `Each effect HOPT(X).` 
- `You can only use this effect of "X" once per turn`										 - `This effect HOPT(X).`
- `You can only use each of the following effects of "X" once per turn`						 - `Each of the following effects HOPT(X).` 
- `You can only gain this effect once per turn`												 - `Gain this effect HOPT(X).`
- `You can only use 1 of the following effects of "X" per turn, and only once that turn.`	 - `Only 1 of following effects HOPT(X).`
- `You can only use 1 of these effects of "X" per turn, and only once that turn.`			 - `Only 1 of these effects HOPT(X).`
- `You can only use 1 "X" efect per turn, and only once that turn.`							 - `Only 1 effect HOPT(X).`
- `You can only use 1 efect of "X" per turn, and only once that turn.`						 - `Only 1 effect HOPT(X).`
- You can only activate 1 "X" per turn.
```