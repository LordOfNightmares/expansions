--Fang of Critias
local m=11082056
local cm=_G["c"..m]
function cm.initial_effect(c)
	aux.AddCodeList(c,89631139)
	--spsummon
	c:EnableReviveLimit()
	--type
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_MONSTER_SSET)
	e0:SetValue(TYPE_SPELL)
	c:RegisterEffect(e0)
	--Dragon/Effect
	local e01=Effect.CreateEffect(c)
	e01:SetType(EFFECT_TYPE_SINGLE)
	e01:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e01:SetCode(EFFECT_ADD_TYPE)
	e01:SetValue(TYPE_EFFECT+TYPE_MONSTER)
	c:RegisterEffect(e01)
	--Activate monster
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	e1:SetCost(cm.cost)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
	--Activate spell
	local e1a=Effect.CreateEffect(c)
	e1a:SetType(EFFECT_TYPE_ACTIVATE)
	e1a:SetCode(EVENT_FREE_CHAIN)
	e1a:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1a:SetDescription(aux.Stringid(m,0))
	e1a:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	-- e1a:SetRange(LOCATION_HAND+LOCATION_SZONE)
	e1a:SetCost(cm.cost)
	e1a:SetOperation(cm.activate)
	c:RegisterEffect(e1a)
	--Activate2 monster
	local e1a2=Effect.CreateEffect(c)
	e1a2:SetType(EFFECT_TYPE_IGNITION)
	e1a2:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	e1a2:SetDescription(aux.Stringid(m,1))
	e1a2:SetRange(LOCATION_MZONE)
	e1a2:SetCost(cm.cost2)
	e1a2:SetOperation(cm.activate2)
	c:RegisterEffect(e1a2)
	--Activate2 spell
	local e1a3=Effect.CreateEffect(c)
	e1a3:SetType(EFFECT_TYPE_ACTIVATE)
	e1a3:SetCode(EVENT_FREE_CHAIN)
	e1a3:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	e1a3:SetDescription(aux.Stringid(m,1))
	e1a3:SetRange(LOCATION_HAND+LOCATION_SZONE)
	e1a3:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1a3:SetCost(cm.cost2)
	e1a3:SetCondition(function(e,tp) return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 end)
	e1a3:SetOperation(cm.activate2)
	c:RegisterEffect(e1a3)
	--add code
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_ADD_CODE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e2:SetValue(10000060)
	c:RegisterEffect(e2)
	--special summon
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetDescription(2)
	e3:SetCode(EFFECT_SPSUMMON_PROC)
	e3:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e3:SetRange(LOCATION_HAND)
	e3:SetCondition(cm.spcon)
	e3:SetOperation(cm.spop)
	c:RegisterEffect(e3)
	--spson
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e4:SetCode(EFFECT_SPSUMMON_CONDITION)
	e4:SetValue(aux.FALSE)
	c:RegisterEffect(e4)
end
local effect_count=0
function cm.tgfilter(c,e,tp)
	return c:IsType(TYPE_TRAP) and Duel.IsExistingMatchingCard(cm.spfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp,c:GetCode(),c)
end
function cm.spfilter(c,e,tp,code,tc)
	return c:IsType(TYPE_FUSION) and c.material_trap and c:IsCanBeSpecialSummoned(e,0,tp,true,false) and code==c.material_trap
		and (Duel.GetLocationCountFromEx(tp,tp,tc,c)>0 or e:GetHandler():IsLocation(LOCATION_MZONE))
end
function cm.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.tgfilter,tp,LOCATION_HAND+LOCATION_SZONE,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
	local g=Duel.SelectMatchingCard(tp,cm.tgfilter,tp,LOCATION_HAND+LOCATION_SZONE,0,1,1,nil,e,tp)
	local tc=g:GetFirst()
	if tc and not tc:IsImmuneToEffect(e) then
		if tc:IsOnField() and tc:IsFacedown() then Duel.ConfirmCards(1-tp,tc) end
		Duel.SendtoGrave(tc,REASON_EFFECT)
		e:SetLabelObject(tc)
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
-- function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
-- 	if chk==0 then return Duel.IsExistingMatchingCard(cm.tgfilter,tp,LOCATION_HAND+LOCATION_SZONE,0,1,nil,e,tp) end
-- 	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
-- end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	Duel.SendtoGrave(e:GetHandler(),REASON_COST)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
	local g=Duel.SelectMatchingCard(tp,cm.tgfilter,tp,LOCATION_HAND+LOCATION_SZONE,0,1,1,nil,e,tp)
	local tc=g:GetFirst()
	local tc=e:GetLabelObject()
	local code=tc:GetCode()
	if not tc:IsLocation(LOCATION_GRAVE) then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local sg=Duel.SelectMatchingCard(tp,cm.spfilter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp,code,nil)
	local sc=sg:GetFirst()
	if sc then
		Duel.BreakEffect()
		Duel.SpecialSummon(sc,0,tp,tp,true,true,POS_FACEUP)
		sc:CompleteProcedure()
	end
end
function cm.cfilter(c)
	return c:GetType()==TYPE_TRAP
end
function cm.cost2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.cfilter,tp,LOCATION_HAND+LOCATION_SZONE,0,1,nil,e) end
	local g=Duel.SelectMatchingCard(tp,cm.cfilter,tp,LOCATION_HAND+LOCATION_SZONE,0,1,1,nil,e)
	local tc=g:GetFirst()
	if tc:IsOnField() and tc:IsFacedown() then Duel.ConfirmCards(1-tp,tc) end
	Duel.SendtoGrave(g,REASON_COST)
	e:SetLabelObject(tc)
end
function cm.activate2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tcc=e:GetLabelObject()	
	if not c:IsLocation(LOCATION_MZONE) and c:IsRelateToEffect(e) and Duel.SpecialSummon(c,0,tp,tp,true,true,POS_FACEUP)~=0 then
		c:CompleteProcedure()
	end
	local vars={}
	if global_card_effect_table[tcc] then
		for k,eff in pairs(global_card_effect_table[tcc]) do
			if eff:GetType()&EFFECT_TYPE_IGNITION>0 
			   or eff:GetType()&EFFECT_TYPE_TRIGGER_F>0 
			   or eff:GetType()&EFFECT_TYPE_TRIGGER_O>0 
			   or eff:GetType()&EFFECT_TYPE_QUICK_F>0
			   or eff:GetType()&EFFECT_TYPE_QUICK_O>0
			   or eff:GetType()&EFFECT_TYPE_ACTIVATE>0 then	
				local reset,rcount=0,0
				if eff~=nil and eff:GLGetReset() then	
					local reset,rcount=eff:GLGetReset()
				end
				if reset&(RESET_EVENT+RESETS_STANDARD)~=0 then
					new_reset = reset
				else
					new_reset = RESET_EVENT+RESETS_STANDARD+reset
				end				
				local cond=eff:GetCondition()
				vars["e"..k]=eff:Clone()
				vars["e"..k]:SetCondition(function(e,tp,eg,ep,ev,re,r,rp,chk)
						-- Debug.Message(string.format("Critias: %s, %s",c:GetCode(),m))
						local count=c:IsOnField()
						local ans=false
						if cond then 	
							ans= cond(e,tp,eg,ep,ev,re,r,rp,chk) and count
						else
							ans= count
						end 
						if not count then e:Reset() return false end
						return ans
					end)
				-- local code=c:GetOriginalCode()
				if c:GetOriginalCode()==m then
					vars["e"..k]:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
				else 
					vars["e"..k]:SetCountLimit(1,m+100,EFFECT_COUNT_CODE_SINGLE)
				end
				local type=EFFECT_TYPE_FIELD
				vars["e"..k]:SetDescription(aux.Stringid(m,effect_count+2))
				if eff:GetType()&EFFECT_TYPE_SINGLE>0 then type=EFFECT_TYPE_SINGLE end
				vars["e"..k]:SetType(type+EFFECT_TYPE_QUICK_O)
				vars["e"..k]:SetRange(LOCATION_MZONE)
				if  eff:GLGetReset()~=nil then vars["e"..k]:SetReset(new_reset,rcount) end
				c:RegisterEffect(vars["e"..k],true)	
				effect_count=effect_count+1			
			end
		end
	end
end
function cm.cfilter2(c,code)
	return c:GetType()==TYPE_TRAP and c:GetCode()==code
end
function cm.target2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.cfilter2,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE,0,1,nil,e:GetLabel()) end
end
function cm.xfilter(c,tp)
	return c:IsType(TYPE_MONSTER) and c:IsAbleToDeckAsCost() and c:GetCode()==89631139 
end
function cm.spcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(cm.xfilter,tp,LOCATION_GRAVE,0,1,nil,tp) 
	and e:GetHandler():IsCanBeSpecialSummoned(e,0,tp,true,true) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0
end
function cm.spop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(tp,cm.xfilter,tp,LOCATION_GRAVE,0,1,1,nil,tp)
	Duel.SendtoDeck(g,nil,1,REASON_COST)
	Duel.ShuffleDeck(tp)
	if c:IsRelateToEffect(e) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and Duel.SpecialSummon(c,0,tp,tp,true,true,POS_FACEUP)~=0 then
		c:CompleteProcedure()
	end
end