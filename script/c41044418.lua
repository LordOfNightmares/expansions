--Millennium Revelation
function c41044418.initial_effect(c)
	aux.AddCodeList(c,10000000,10000010,10000020)
	--activate
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	c:RegisterEffect(e0)
	--to hand
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(41044418,0))
	e1:SetCategory(CATEGORY_TOHAND+CATEGORY_SEARCH)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_SZONE)
	e1:SetCountLimit(1,41044418)
	e1:SetCost(c41044418.thcost)
	e1:SetTarget(c41044418.thtg)
	e1:SetOperation(c41044418.thop)
	c:RegisterEffect(e1)
	--togy
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e2:SetDescription(aux.Stringid(41044418,1))
	e2:SetCode(EVENT_LEAVE_FIELD)
	e2:SetCountLimit(1,41044418+100)
	e2:SetTarget(c41044418.rthtg)
	e2:SetOperation(c41044418.rthop)
	c:RegisterEffect(e2)
	-- --keep on field
	-- local e3=Effect.CreateEffect(c)
    -- e3:SetType(EFFECT_TYPE_FIELD)
    -- e3:SetCode(41044418)
    -- e3:SetRange(LOCATION_SZONE)
    -- e3:SetTargetRange(LOCATION_MZONE,0)
    -- c:RegisterEffect(e3)
	--Gods can't activate Endphase
	local e00=Effect.CreateEffect(c)
	e00:SetType(EFFECT_TYPE_FIELD)
	e00:SetCode(EFFECT_CANNOT_TRIGGER)
	-- e00:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e00:SetCondition(function() return Duel.GetCurrentPhase()==PHASE_END end)
	-- e00:SetReset(RESET_EVENT+RESETS_STANDARD)
	e00:SetRange(LOCATION_SZONE)
	e00:SetTargetRange(LOCATION_MZONE,0)
	e00:SetTarget(c41044418.limtg)
	c:RegisterEffect(e00)
end

function c41044418.limtg(e,c)
    return c:IsCode(10000000,10000010,10000020)
end
function c41044418.costfilter(c)
	return c:IsAbleToGraveAsCost() and c:IsAttribute(ATTRIBUTE_DIVINE)
end
function c41044418.thcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c41044418.costfilter,tp,LOCATION_HAND,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local g=Duel.SelectMatchingCard(tp,c41044418.costfilter,tp,LOCATION_HAND,0,1,1,nil)
	Duel.SendtoGrave(g,REASON_COST)
end
function c41044418.thfilter(c)
	return c:IsCode(83764718) and c:IsAbleToHand()
end
function c41044418.thtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c41044418.thfilter,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,nil) end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK+LOCATION_GRAVE)
end
function c41044418.thop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,aux.NecroValleyFilter(c41044418.thfilter),tp,LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,g)
	end
end
function c41044418.rbop1(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsAbleToGrave() end
	Duel.SendtoGrave(e:GetHandler(),REASON_EFFECT)
end
function c41044418.rfilter(c)
	return not c:IsCode(41044418) and (c:IsCode(10000000) or c:IsCode(10000010) or c:IsCode(10000020) or aux.IsCodeListed(c,10000000) or aux.IsCodeListed(c,10000010) or aux.IsCodeListed(c,10000020)) and c:IsAbleToHand()
end
function c41044418.rthtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(aux.NecroValleyFilter(c41044418.rfilter),tp,LOCATION_DECK+LOCATION_GRAVE,0,1,nil) end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK+LOCATION_GRAVE)
end
function c41044418.rthop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,aux.NecroValleyFilter(c41044418.rfilter),tp,LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,g)
	end
end
