EFFECT_FLAG2_AVAILABLE_BD           = 0x2000000 --The effect is also applicable when the battle damage is confirmed
PENDULUM_CHECKLIST                  = Auxiliary.PendulumChecklist
RACE_DIVINE                         = 0x200000
EFFECT_FLAG2_ACTIVATE_MONSTER_SZONE = 0x0400
-- RUSH_EVENT                          = EVENT_CUSTOM+515959000 --necessary for rush summoning

--Duel Effects without player target range
DUEL_EFFECT_NOP                     = {EFFECT_DISABLE_FIELD}
self_reference_effect				= nil