--Claw of Hermes
local m=46232525
local cm=_G["c"..m]
function cm.initial_effect(c)
	aux.AddCodeList(c,74677422)
	--spsummon
	c:EnableReviveLimit()
	--type
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_MONSTER_SSET)
	e0:SetValue(TYPE_SPELL)
	c:RegisterEffect(e0)
	--Dragon/Effect
	local e01=Effect.CreateEffect(c)
	e01:SetType(EFFECT_TYPE_SINGLE)
	e01:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e01:SetCode(EFFECT_ADD_TYPE)
	e01:SetValue(TYPE_EFFECT+TYPE_MONSTER)
	c:RegisterEffect(e01)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	e1:SetCost(cm.cost)
	-- e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
	local e1a=Effect.CreateEffect(c)
	e1a:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1a:SetDescription(aux.Stringid(m,0))
	e1a:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	e1a:SetType(EFFECT_TYPE_ACTIVATE)
	e1a:SetCode(EVENT_FREE_CHAIN)
	e1a:SetCost(cm.cost)
	e1a:SetOperation(cm.activate)
	c:RegisterEffect(e1a)
	--Activate2
	local e1a2=Effect.CreateEffect(c)
	e1a2:SetType(EFFECT_TYPE_IGNITION)
	e1a2:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	e1a2:SetDescription(aux.Stringid(m,1))
	e1a2:SetCategory(CATEGORY_EQUIP)
	e1a2:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_CONTINUOUS_TARGET)
	e1a2:SetRange(LOCATION_MZONE+LOCATION_SZONE)
	e1a2:SetCost(cm.cost2)
	e1a2:SetTarget(cm.target2)
	e1a2:SetOperation(cm.activate2)
	c:RegisterEffect(e1a2)
	local e1a3=Effect.CreateEffect(c)
	e1a3:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_CONTINUOUS_TARGET)
	e1a3:SetType(EFFECT_TYPE_ACTIVATE)
	e1a3:SetCode(EVENT_FREE_CHAIN)
	e1a3:SetCountLimit(1,m,EFFECT_COUNT_CODE_SINGLE)
	e1a3:SetDescription(aux.Stringid(m,1))
	e1a3:SetCategory(CATEGORY_EQUIP)
	e1a3:SetCost(cm.cost2)
	e1a3:SetTarget(cm.target2)
	e1a3:SetOperation(cm.activate2)
	c:RegisterEffect(e1a3)
	--add code
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_ADD_CODE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e2:SetValue(10000070)
	c:RegisterEffect(e2)
    --special summon
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetDescription(2)
	e3:SetCode(EFFECT_SPSUMMON_PROC)
	e3:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e3:SetRange(LOCATION_GRAVE)
	e3:SetCondition(cm.spcon)
	e3:SetOperation(cm.spop)
	c:RegisterEffect(e3)
	--spson
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e4:SetCode(EFFECT_SPSUMMON_CONDITION)
	e4:SetValue(aux.FALSE)
	c:RegisterEffect(e4)
end
function cm.xfilter(c,tp)
	return c:IsType(TYPE_MONSTER) and c:IsAbleToGraveAsCost() and c:GetCode()==74677422 
end
function cm.spcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(cm.xfilter,tp,LOCATION_MZONE,0,1,nil,tp) and e:GetHandler():IsCanBeSpecialSummoned(e,0,tp,true,false)
end
function cm.spop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local g=Duel.SelectMatchingCard(tp,cm.xfilter,tp,LOCATION_MZONE,0,1,1,nil,tp)
	Duel.SendtoGrave(g,REASON_COST)
	if c:IsRelateToEffect(e) and Duel.SpecialSummon(c,0,tp,tp,true,false,POS_FACEUP)~=0 then
		c:CompleteProcedure()
	end
end
function cm.tgfilter(c,e,tp)
	return c:IsType(TYPE_MONSTER) and Duel.IsExistingMatchingCard(cm.spfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp,c:GetRace(),mc)
end
function cm.spfilter(c,e,tp,race,mc)
	return c:IsType(TYPE_FUSION) and c.material_race and c:IsCanBeSpecialSummoned(e,0,tp,true,false) and race==c.material_race
		and Duel.GetLocationCountFromEx(tp,tp,mc,c)>0
end
function cm.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.tgfilter,tp,LOCATION_HAND+LOCATION_MZONE,0,1,e:GetHandler(),e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
	local g=Duel.SelectMatchingCard(tp,cm.tgfilter,tp,LOCATION_HAND+LOCATION_MZONE,0,1,1,e:GetHandler(),e,tp)
	local tc=g:GetFirst()
	if tc and not tc:IsImmuneToEffect(e) then
		if tc:IsOnField() and tc:IsFacedown() then Duel.ConfirmCards(1-tp,tc) end
		
		Duel.SendtoGrave(tc,REASON_EFFECT)
		e:SetLabelObject(tc)
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
-- function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
-- 	if chk==0 then return Duel.IsExistingMatchingCard(cm.spfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp,race,nil) end
-- 	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
-- end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	Duel.SendtoGrave(e:GetHandler(),REASON_COST)
	local tc=e:GetLabelObject()
	local race=tc:GetRace()
	if not tc:IsLocation(LOCATION_GRAVE) then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local sg=Duel.SelectMatchingCard(tp,cm.spfilter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp,race,nil)
	local sc=sg:GetFirst()
	if sc then
		Duel.BreakEffect()
		Duel.SpecialSummon(sc,0,tp,tp,true,false,POS_FACEUP)
		sc:CompleteProcedure()
	end

end
function cm.cfilter(c)
	return c:IsType(TYPE_MONSTER)
end
function cm.cost2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.cfilter,tp,LOCATION_HAND+LOCATION_MZONE,0,1,e:GetHandler(),e) end
	local g=Duel.SelectMatchingCard(tp,cm.cfilter,tp,LOCATION_HAND+LOCATION_MZONE,0,1,1,e:GetHandler(),e)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	Duel.SendtoGrave(g,REASON_COST)
	e:SetLabelObject(g:GetFirst())
	local c=e:GetHandler()
	local cid=Duel.GetChainInfo(0,CHAININFO_CHAIN_ID)
	local re0=Effect.CreateEffect(c)
	re0:SetType(EFFECT_TYPE_SINGLE)
	re0:SetCode(EFFECT_REMAIN_FIELD)
	re0:SetProperty(EFFECT_FLAG_OATH)
	re0:SetReset(RESET_CHAIN)
	c:RegisterEffect(re0)
	local re1=Effect.CreateEffect(c)
	re1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	re1:SetCode(EVENT_CHAIN_DISABLED)
	re1:SetOperation(cm.tgop)
	re1:SetLabel(cid)
	re1:SetReset(RESET_CHAIN)
	Duel.RegisterEffect(re1,tp)
end
function cm.findfilt(c,tc)
	return c==tc
end
function cm.tgop(e,tp,eg,ep,ev,re,r,rp)
	local cid=Duel.GetChainInfo(ev,CHAININFO_CHAIN_ID)
	if cid~=e:GetLabel() then return end
	if e:GetOwner():IsRelateToChain(ev) then
		e:GetOwner():CancelToGrave(false)
	end
end
function cm.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsFaceup()
end
function cm.target2(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and cm.filter(chkc) end
	if chk==0 then return (e:IsHasType(EFFECT_TYPE_ACTIVATE) or e:IsHasType(EFFECT_TYPE_IGNITION))
		and Duel.IsExistingTarget(cm.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,e:GetHandler()) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EQUIP)
	Duel.SelectTarget(tp,cm.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,e:GetHandler())
	Duel.SetOperationInfo(0,CATEGORY_EQUIP,e:GetHandler(),1,0,0)
end
function cm.activate2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	local tcc=e:GetLabelObject()
	Duel.Equip(tp,c,tc)
	--equip
	local ex=Effect.CreateEffect(c)
	ex:SetType(EFFECT_TYPE_SINGLE)
	ex:SetCode(EFFECT_EQUIP_LIMIT)
	ex:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	ex:SetValue(function(e,c) return c==e:GetLabelObject() end)
	ex:SetLabelObject(tc)
	c:RegisterEffect(ex)
	local vars={}
	if global_card_effect_table[tcc] then
		for k,eff in pairs(global_card_effect_table[tcc]) do
			if eff:GetType()&EFFECT_TYPE_IGNITION>0 
			   or eff:GetType()&EFFECT_TYPE_TRIGGER_F>0 
			   or eff:GetType()&EFFECT_TYPE_TRIGGER_O>0 
			   or eff:GetType()&EFFECT_TYPE_QUICK_F>0
			   or eff:GetType()&EFFECT_TYPE_QUICK_O>0
			   or eff:GetType()&EFFECT_TYPE_ACTIVATE>0 then	
				local reset,rcount=0,0
				if eff~=nil and eff:GLGetReset() then	
					local reset,rcount=eff:GLGetReset()
				end
				if reset&(RESET_EVENT+RESETS_STANDARD)~=0 then
					new_reset = reset
				else
					new_reset = RESET_EVENT+RESETS_STANDARD+reset
				end
				local cond=eff:GetCondition()
				vars["e"..k]=eff:Clone()
				vars["e"..k]:SetCondition(function(e,tp,eg,ep,ev,re,r,rp,chk)
						-- Debug.Message(string.format("Hermos:  %s, %s ,%s = %s",tcc:GetCode(),tc:GetCode(),m,m+tc:GetCode()))
						local g=e:GetHandler():GetEquipGroup()
						local count=#g:Filter(cm.findfilt, nil,c)>0
						local ans=false
						if cond then 
							ans= cond(e,tp,eg,ep,ev,re,r,rp,chk) and count
						else
							ans= count
						end 
						if not count then e:Reset() return false end
						return ans
					end)
					-- local use,climit,hopt,x,y=eff:GetCountLimit()
				vars["e"..k]:SetCountLimit(1,tcc:GetCode()+100,EFFECT_COUNT_CODE_SINGLE)
				if eff:GLGetReset()~=nil then vars["e"..k]:SetReset(new_reset,rcount) end
				tc:RegisterEffect(vars["e"..k],true)				
			end
		end
	end
end
