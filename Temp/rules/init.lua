Duel.LoadScript("CustomConstants.lua")
Duel.LoadScript("masterRule.lua")
--Mega RegisterEffect
function Auxiliary.RegisterEffect(e,c,tp)
    c:RegisterEffect(e)
    Duel.RegisterEffect(e,tp)
end
--GetID implementation
-- function GetID()
--     local str=string.match(debug.getinfo(2,'S')['source'],"c%d+%.lua")
--     str=string.sub(str,1,string.len(str)-4)
--     local cod=_G[str]
--     local id=tonumber(string.sub(str,2))
--     return cod,id
-- end
-- function GetID()
-- 	local offset=self_code<100000000 and 1 or 100
-- 	return self_table,self_code,offset
-- end
function getID() return GetID() end
function getid() return GetID() end
function count(base, pattern)
    return select(2, string.gsub(base, pattern, ""))
end
function int2bin(n)
  local result = {}
  while n ~= 0 do
    if n % 2 == 0 then
      result[#result+1] = '0'
    else
      result[#result+1] = '1'
    end
    n = math.floor(n / 2)
  end
  return table.concat(result)
end
function getArgs(fun)
	local args = {}
	local hook = debug.gethook()
	local i = 1
	local name, value
	local argHook = function( ... )
		local info = debug.getinfo(3)
		if 'pcall' ~= info.name then return end
		while( true ) do
			name, value = debug.getlocal( 2, i )
			if ( name == nil ) then break end
			if '(*temporary)' == name then
				debug.sethook(hook)
				error('')
				return
			end
			args[ i ] = name
			i = i + 1
		end
	end
	
	debug.sethook(argHook, "c")
	pcall(fun)
	
	return args
end
-- function getArgs(fun)
-- 	local hook = debug.gethook()
-- 	local name, value
-- 	local NIL = {} -- to represent nil variables
-- 	local args = {}
-- 	local i = 1
-- 	local argHook = function( ... )
-- 		local info = debug.getinfo(3)
-- 		if 'pcall' ~= info.name then return end

-- 		while( true ) do
-- 			name, value = debug.getlocal( 2, i )
-- 			if ( name == nil ) then break end
-- 			if '(*temporary)' == name then
-- 				debug.sethook(hook)
-- 				error('')
-- 				return
-- 			end
-- 			args[ i ] = name
-- 			i = i + 1
-- 		end
-- 	end
-- 	debug.sethook(argHook, "c")
-- 	pcall(fun)
-- 	return args
-- end
--Global Card Effect Table
if not global_card_effect_table_global_check then
--[[Example
    function cannot_prevention(c)
      if global_card_effect_table[c] then
            for key,value in pairs(global_card_effect_table[c]) do
                if (value:GetCode()==EFFECT_CANNOT_ACTIVATE 
                 or value:GetCode()==EFFECT_DISABLE)
                and value:IsHasProperty(EFFECT_FLAG_PLAYER_TARGET) 
                and value:GetType()==EFFECT_TYPE_FIELD then
                 Debug.Message(c:GetCode())
                 return c
                end
            end
        end 
    end]]
    global_card_effect_table_global_check=true
    global_card_effect_table={}
    Card.register_global_card_effect_table = Card.RegisterEffect
    function Card:RegisterEffect(e)
        if not global_card_effect_table[self] then global_card_effect_table[self]={} end
        table.insert(global_card_effect_table[self],e)
        self.register_global_card_effect_table(self,e)
    end
end
function ToBool(vaule)
    return vaule~= (nil or 0)
end
--c++
-- check_param_count(L, 6);
-- check_param(L, PARAM_TYPE_CARD, 1);
-- check_param(L, PARAM_TYPE_EFFECT, 2);
-- card* pcard = *(card**) lua_touserdata(L, 1);
-- effect* peffect = *(effect**) lua_touserdata(L, 2);
-- uint32 sumtype = (uint32)lua_tointeger(L, 3);
-- uint32 sumplayer = (uint32)lua_tointeger(L, 4);
-- uint32 nocheck = lua_toboolean(L, 5);
-- uint32 nolimit = lua_toboolean(L, 6);
-- uint32 sumpos = POS_FACEUP;
-- uint32 toplayer = sumplayer;
-- uint32 zone = 0xff;
-- if(lua_gettop(L) >= 7)
--     sumpos = (uint32)lua_tointeger(L, 7);
-- if(lua_gettop(L) >= 8)
--     toplayer = (uint32)lua_tointeger(L, 8);
-- if(lua_gettop(L) >= 9)
--     zone = (uint32)lua_tointeger(L, 9);
-- if(pcard->is_can_be_special_summoned(peffect, sumtype, sumpos, sumplayer, toplayer, nocheck, nolimit, zone))
--     lua_pushboolean(L, 1);
-- else
--     lua_pushboolean(L, 0);
-- return 1;
-- function Card.IsCanBeSpecialSummoned(...)
--     local params={...}
--     function params.check_param_count(table,count)
--         if #table < count then
--             print(string.format("%d Parameters are needed.", count))
--             return true
--         end
--     end
--     params:check_param_count(6)
--     local pcard = params[1]
--     local peffect = params[2]
--     local sumtype = tonumber(params[3])
--     local sumplayer = tonumber(params[4])
--     local nocheck = ToBool(params[5])
--     local nolimit = ToBool(params[6])
--     local sumpos = POS_FACEUP
--     local toplayer = sumplayer
--     local zone = 0xff;
--     if params[7] then
--         sumpos = tonumber(params[7])
--     end
--     if params[8] then
--         toplayer = tonumber(params[8])
--     end
--     if params[9] then
--         zone = tonumber(params[9])
--     end
--     --[[insert rules here]]
--     if pcard:IsType(TYPE_LINK) then return false end
--     return true
-- end
-- function Card.IsCanBeSpecialSummoned(...)
--     local params={...}
--     function params.check_param_count(table,count)
--         if #table < count then
--             print(string.format("%d Parameters are needed.", count))
--             return true
--         end
--     end
--     params:check_param_count(6)
--     local pcard = params[1]
--     local peffect = params[2]
--     local sumtype = tonumber(params[3])
--     local sumplayer = tonumber(params[4])
--     local nocheck = ToBool(params[5])
--     local nolimit = ToBool(params[6])
--     local sumpos = POS_FACEUP
--     local toplayer = sumplayer
--     local zone = 0xff;
--     if params[7] then
--         sumpos = tonumber(params[7])
--     end
--     if params[8] then
--         toplayer = tonumber(params[8])
--     end
--     if params[9] then
--         zone = tonumber(params[9])
--     end
--     --[[insert rules here]]
--     if pcard:IsType(TYPE_LINK) then return false end
--     return true
-- end
--overwrite functions
local is_type, card_remcounter, duel_remcounter, effect_set_target_range, effect_set_reset, add_xyz_proc, add_xyz_proc_nlv, duel_overlay, duel_set_lp, duel_select_target, duel_banish, card_check_remove_overlay_card, is_reason, duel_check_tribute, select_tribute,card_sethighlander,
	card_is_facedown, card_is_able_to_remove, card_is_able_to_remove_as_cost, card_is_able_to_hand, card_is_can_be_ssed, card_get_overlay_group, card_get_overlay_count, duel_get_overlay_group, duel_get_overlay_count = 
	
	Card.IsType, Card.RemoveCounter, Duel.RemoveCounter, Effect.SetTargetRange, Effect.SetReset, Auxiliary.AddXyzProcedure, Auxiliary.AddXyzProcedureLevelFree, Duel.Overlay, Duel.SetLP, Duel.SelectTarget, Duel.Remove, Card.CheckRemoveOverlayCard, Card.IsReason, Duel.CheckTribute, Duel.SelectTribute, Card.SetUniqueOnField,
	Card.IsFacedown, Card.IsAbleToRemove, Card.IsAbleToRemoveAsCost, Card.IsAbleToHand, Card.IsCanBeSpecialSummoned, Card.GetOverlayGroup, Card.GetOverlayCount, Duel.GetOverlayGroup, Duel.GetOverlayCount

-----------------------------------------------------------------------------------------------------------------------
--get Effect RESET
global_reset_effect_table={}
Effect.SetReset=function(e,reset,rct)
	local rct=rct or 1
	global_reset_effect_table[e]={reset,rct}
	return effect_set_reset(e,reset,rct)
end
function Effect.GLGetReset(e)
	if not global_reset_effect_table[e] then return 0,0 end
	local reset=global_reset_effect_table[e][1]
	local rct=global_reset_effect_table[e][2]
	return reset,rct
end
function Auxiliary.SetOperationResultAsLabel(op)
	return	function(e,tp,eg,ep,ev,re,r,rp)
				local res=op(e,tp,eg,ep,ev,re,r,rp)
				e:SetLabel(res)
				return res
			end
end
--Card EFFECT TABLES
Auxiliary.kaiju_procs={}
global_target_range_effect_table={}
Effect.SetTargetRange=function(e,self,oppo)
	global_target_range_effect_table[e]={self,oppo}
	if e:GetCode()==EFFECT_SPSUMMON_PROC or e:GetCode()==EFFECT_SPSUMMON_PROC_G then
		if oppo==1 then
			table.insert(Auxiliary.kaiju_procs,e)
		end
	end
	return effect_set_target_range(e,self,oppo)
end
function Effect.GLGetTargetRange(e)
	if not global_target_range_effect_table[e] then return 0,0 end
	local s=global_target_range_effect_table[e][1]
	local o=global_target_range_effect_table[e][2]
	return s,o
end
global_reset_effect_table={}
Effect.SetReset=function(e,reset,rct)
	local rct=rct or 1
	global_reset_effect_table[e]={reset,rct}
	return effect_set_reset(e,reset,rct)
end

-- --Global Card Effect Table
-- if not global_card_effect_table_global_check then
-- 	global_card_effect_table_global_check=true
-- 	global_card_effect_table={}
-- 	Card.register_global_card_effect_table = Card.RegisterEffect
-- 	function Card:RegisterEffect(e,forced)
-- 		if not global_card_effect_table[self] then global_card_effect_table[self]={} end
-- 		table.insert(global_card_effect_table[self],e)
-- 		if e:GetCode()==EFFECT_DISABLE_FIELD and e:GetLabel()==0 and e:GetOperation() then
-- 			local op=e:GetOperation()
-- 			e:SetOperation(Auxiliary.SetOperationResultAsLabel(op))
-- 		end
-- 		local condition,cost,tg,op=e:GetCondition(),e:GetCost(),e:GetTarget(),e:GetOperation()
-- 		if cost and not (e:GetType()==EFFECT_TYPE_FIELD or e:GetType()==EFFECT_TYPE_SINGLE) then
-- 			local newcost =	function(e,tp,eg,ep,ev,re,r,rp,chk)
-- 								if chk==0 then
-- 									self_reference_effect=e
-- 									return cost(e,tp,eg,ep,ev,re,r,rp,chk)
-- 								end
-- 								self_reference_effect=e
-- 								cost(e,tp,eg,ep,ev,re,r,rp,chk)
-- 							end
-- 			e:SetCost(newcost)
-- 		end
-- 		if tg and not (e:GetType()==EFFECT_TYPE_FIELD or e:GetType()==EFFECT_TYPE_SINGLE) then
-- 			local newtg =	function(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
-- 								if chk==0 then
-- 									self_reference_effect=e
-- 									return tg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
-- 								end
-- 								self_reference_effect=e
-- 								tg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
-- 							end
-- 			e:SetTarget(newtg)
-- 		end
-- 		if op and not (e:GetType()==EFFECT_TYPE_FIELD or e:GetType()==EFFECT_TYPE_SINGLE) then
-- 			local newop =	function(e,tp,eg,ep,ev,re,r,rp)
-- 								self_reference_effect=e
-- 								op(e,tp,eg,ep,ev,re,r,rp)
-- 							end
-- 			e:SetOperation(newop)
-- 		end
-- 		return self.register_global_card_effect_table(self,e,forced)
-- 	end
-- end

-- --Global Card Effect Table (for Duel.RegisterEffect)
-- if not global_duel_effect_table_global_check then
-- 	global_duel_effect_table_global_check=true
-- 	global_duel_effect_table={}
-- 	Duel.register_global_duel_effect_table = Duel.RegisterEffect
-- 	Duel.RegisterEffect = function(e,tp)
-- 							if not global_duel_effect_table[tp] then global_duel_effect_table[tp]={} end
-- 							table.insert(global_duel_effect_table[tp],e)
-- 							local s,o=e:GLGetTargetRange()
-- 							if not e:IsHasProperty(EFFECT_FLAG_PLAYER_TARGET) and s==0 and o==0 then
-- 								for i=1,#DUEL_EFFECT_NOP do
-- 									if e:GetCode()==DUEL_EFFECT_NOP[i] then e:SetProperty(e:GetProperty()|EFFECT_FLAG_PLAYER_TARGET) e:SetTargetRange(1,0) end
-- 								end
-- 							end
-- 							local condition,cost,tg,op=e:GetCondition(),e:GetCost(),e:GetTarget(),e:GetOperation()
-- 							if cost and not (e:GetType()==EFFECT_TYPE_FIELD or e:GetType()==EFFECT_TYPE_SINGLE) then
-- 								local newcost =	function(e,tp,eg,ep,ev,re,r,rp,chk)
-- 													if chk==0 then
-- 														self_reference_effect=e
-- 														return cost(e,tp,eg,ep,ev,re,r,rp,chk)
-- 													end
-- 													self_reference_effect=e
-- 													cost(e,tp,eg,ep,ev,re,r,rp,chk)
-- 												end
-- 								e:SetCost(newcost)
-- 							end
-- 							if tg and not (e:GetType()==EFFECT_TYPE_FIELD or e:GetType()==EFFECT_TYPE_SINGLE) then
-- 								local newtg =	function(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
-- 													if chk==0 then
-- 														self_reference_effect=e
-- 														return tg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
-- 													end
-- 													self_reference_effect=e
-- 													tg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
-- 												end
-- 								e:SetTarget(newtg)
-- 							end
-- 							if op and not (e:GetType()==EFFECT_TYPE_FIELD or e:GetType()==EFFECT_TYPE_SINGLE) then
-- 								local newop =	function(e,tp,eg,ep,ev,re,r,rp)
-- 													self_reference_effect=e
-- 													op(e,tp,eg,ep,ev,re,r,rp)
-- 												end
-- 								e:SetOperation(newop)
-- 							end
-- 							return Duel.register_global_duel_effect_table(e,tp)
-- 	end
-- end
-----------------------------------------------------------------------------------------------------------------------

--MasterRule activation
if Duel.GetFlagEffect(0,515959000)==0 then
    MasterRule.Rule(0)
    Duel.RegisterFlagEffect(0,515959000,0,0,1)
end
if Duel.GetFlagEffect(1,515959000)==0 then
    MasterRule.Rule(1)
    Duel.RegisterFlagEffect(1,515959000,0,0,1)
end
-- Debug.Message('MasterRule - 4.6')

