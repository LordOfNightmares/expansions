-- MasterRule
MasterRule = {}
mr = MasterRule
function MasterRule.Rule(tp)
    -- check for negations
    local e0 = Effect.GlobalEffect()
    e0:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
    e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e0:SetCode(EVENT_CHAINING)
    e0:SetTargetRange(1, 0)
    e0:SetLabel(0)
    e0:SetCondition(function(e, tp, eg, ep, ev, re, r, rp)
        return rp == tp and (re:IsActiveType(TYPE_MONSTER) or (re:IsActiveType(TYPE_SPELL + TYPE_TRAP) and re:IsHasType(EFFECT_TYPE_ACTIVATE)))
                   and (re:IsHasCategory(CATEGORY_DISABLE_SUMMON) or re:IsHasCategory(CATEGORY_NEGATE) or re:IsHasCategory(CATEGORY_DISABLE))
    end)
    e0:SetOperation(function(e, tp, eg, ep, ev, re, r, rp)
        local t = math.floor(Duel.GetTurnCount() / 2)
        local val = t < 2 and 0 or 1
        if Duel.GetFlagEffect(rp, 5159590001) >= val and Duel.IsExistingMatchingCard(Card.IsAbleToDeckAsCost, tp, LOCATION_HAND, 0, 1, nil) then
            local g = Duel.SelectMatchingCard(rp, Card.IsAbleToDeckAsCost, rp, LOCATION_HAND, 0, 1, 1, nil)
            local cost=e0:GetLabel()
            Duel.SendtoDeck(g, nil, cost, REASON_COST)
            e0:SetLabel(cost+1)
        end
        Duel.RegisterFlagEffect(rp, 5159590001, RESET_PHASE + PHASE_END, 0, 0)
    end)
    e0:SetReset(RESET_EVENT+RESET_PHASE+PHASE_END)
    Duel.RegisterEffect(e0, tp)
    -- cannot negate
    local e01 = Effect.GlobalEffect()
    e01:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_PLAYER_TARGET  + EFFECT_FLAG_CANNOT_INACTIVATE)
    e01:SetCode(EFFECT_CANNOT_ACTIVATE)
    e01:SetTargetRange(1, 0)
    e01:SetValue(function(e, re, rp, tp)
        local t = math.floor(Duel.GetTurnCount() / 2)
        local val = t < 2 and 0 or 1
        return Duel.GetFlagEffect(rp, 5159590001) >= val and not Duel.IsExistingMatchingCard(Card.IsAbleToDeckAsCost, tp, LOCATION_HAND, 0, 1, nil)
                   and (re:IsActiveType(TYPE_MONSTER) or (re:IsActiveType(TYPE_SPELL + TYPE_TRAP) and re:IsHasType(EFFECT_TYPE_ACTIVATE)))
                   and re:IsHasCategory(CATEGORY_NEGATE + CATEGORY_DISABLE + CATEGORY_DISABLE_SUMMON)
    end)
    Duel.RegisterEffect(e01, tp)
    -- Extra to main
    local e1 = Effect.GlobalEffect()
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_CANNOT_INACTIVATE)
    e1:SetCode(EFFECT_EXTRA_TOMAIN_KOISHI)
    e1:SetTargetRange(1, 0)
    e1:SetValue(1)
    Duel.RegisterEffect(e1, tp)
    -- Extra monsters must not be spsummoned in Extra monster Zone
    local e2 = Effect.GlobalEffect()
    e2:SetType(EFFECT_TYPE_FIELD)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCode(EFFECT_MUST_USE_MZONE)
    e2:SetTargetRange(LOCATION_EXTRA, 0)
    e2:SetValue(MasterRule.func_emz)
    Duel.RegisterEffect(e2, tp)
    -- redraw
    local e3 = Effect.GlobalEffect()
    e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_CANNOT_INACTIVATE)
    e3:SetCode(EVENT_ADJUST)
    e3:SetCountLimit(1)
    e3:SetCondition(function(e) return Duel.GetTurnCount() == 1 end)
    e3:SetTarget(function(e, tp, eg, ep, ev, re, r, rp, chk)
        if chk == 0 then return Duel.IsExistingMatchingCard(aux.TRUE, tp, LOCATION_HAND, 0, 1, nil) end
        Duel.SetTargetPlayer(tp)
        Duel.SetOperationInfo(0, CATEGORY_TODECK, nil, 1, tp, LOCATION_HAND)
    end)
    e3:SetOperation(function(e, tp, eg, ep, ev, re, r, rp)
        if not Duel.SelectYesNo(tp, aux.Stringid(10000000, 3)) then return end
        local p = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER)
        Duel.Hint(HINT_SELECTMSG, p, HINTMSG_TODECK)
        local g = Duel.SelectMatchingCard(p, aux.TRUE, p, LOCATION_HAND, 0, 0, 3, nil)
        if g:GetCount() > 0 then
            local ct = Duel.SendtoDeck(g, nil, 2, REASON_EFFECT)
            Duel.ShuffleDeck(p)
            Duel.BreakEffect()
            Duel.Draw(p, ct, REASON_EFFECT)
        end
    end)
    Duel.RegisterEffect(e3, tp)
    -- --advantage balance on nontarget removal
    -- local abal=Effect.GlobalEffect()
	-- abal:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	-- abal:SetCode(EVENT_CHAIN_SOLVING)
    -- abal:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	-- abal:SetCondition(MasterRule.ntgcon)
    -- abal:SetCondition(MasterRule.ntgop)
    -- Duel.RegisterEffect(abal,tp)
    -- local abal1=Effect.GlobalEffect()
	-- abal1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	-- abal1:SetCode(EVENT_LEAVE_FIELD)
    -- abal1:SetLabelObject(abal)
    -- abal1:SetTargetRange(0,1)
    -- abal1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	-- abal1:SetCondition(function(e,tp,eg,ep,ev,re,r,rp) local label=e:GetLabelObject():GetLabel() return rp==tp and label>0 and Duel.GetFieldGroupCount(1-tp, LOCATION_ONFIELD,0)<label  end)
    -- abal1:SetOperation(MasterRule.ntgop2)
	-- Duel.RegisterEffect(abal1,tp)
end
function MasterRule.ntgfilter(c,tp,ev,re)
    return ev and not Duel.CheckChainTarget(ev,c) and c:IsLocation(LOCATION_ONFIELD) and c:IsControler(1-tp) and re and re:GetHandler():IsControler(tp)
end
function MasterRule.ntgcon(e,tp,eg,ep,ev,re,r,rp) 
    local p1c=Duel.GetFieldGroupCount(tp, LOCATION_HAND+LOCATION_ONFIELD,0)+1
    local p2c=Duel.GetFieldGroupCount(1-tp, LOCATION_HAND+LOCATION_ONFIELD,0)
    local g=Duel.GetMatchingGroup(MasterRule.ntgfilter, tp, 0, LOCATION_ONFIELD,nil,tp,ev,re)
    if p1c>=p2c and rp==tp and #g>0 then 
        return true
    end
    return false
end
function MasterRule.ntgop(e,tp,eg,ep,ev,re,r,rp)  
    local p1c=Duel.GetFieldGroupCount(tp, LOCATION_HAND+LOCATION_ONFIELD,0)+1
    local p2c=Duel.GetFieldGroupCount(1-tp, LOCATION_HAND+LOCATION_ONFIELD,0)
    local g=Duel.GetMatchingGroup(MasterRule.ntgfilter, tp, 0, LOCATION_ONFIELD,nil,tp,ev,re)
    if p1c>=p2c then 
        local opp_f=Duel.GetFieldGroupCount(1-tp, LOCATION_ONFIELD,0)
        local label=0
        -- Debug.Message(string.format("[%s]: %s,%s",tp,#g,opp_f))
        if #g<opp_f then label=#g else label=opp_f end
        e:SetLabel(label) 
    end
end
function MasterRule.ntgop2(e,tp,eg,ep,ev,re,r,rp)  
    local curr_opp_f= Duel.GetFieldGroupCount(1-tp, LOCATION_ONFIELD,0)
    local prev_opp_f= e:GetLabelObject():GetLabel()
    local tdcount=prev_opp_f-curr_opp_f
    -- Debug.Message(e:GetLabelObject():GetLabel()>0 and  rp==tp)
    -- Debug.Message(string.format("+[%s]: %s,%s",tp,prev_opp_f,curr_opp_f))
    local tg = Duel.SelectMatchingCard(tp, Card.IsAbleToDeckAsCost, tp, LOCATION_HAND+LOCATION_ONFIELD, 0, tdcount, tdcount, nil)
    Duel.SendtoDeck(tg, nil, 1, REASON_COST)
end
-- Link monster to only link zones and extra deck monsters out of extra monster zonee
function MasterRule.func_linkfil(c, tp) return c:GetSequence() > 4 and c:IsType(TYPE_LINK) and c:IsControler(tp) end
function MasterRule.func_emz(e, c, fp, rp, r)
    local lval = 0x600060
    local mg = Duel.GetMatchingGroup(MasterRule.func_linkfil, tp, LOCATION_MZONE, 0, nil, tp)
    if mg and mg:GetCount() > 0 then
        local tc = mg:GetFirst()
        while tc do
            lval = tc:GetLinkedZone() | lval
            tc = mg:GetNext()
        end
    end
    if not c:IsType(TYPE_LINK) then
        return 0x9FFF9F
    else
        return lval
    end
end
function Auxiliary.extramonsterzonefiter(c) return c:GetSequence() > 4 end
-- pendulum fix for MasterRule
function Auxiliary.PendOperation()
    return function(e, tp, eg, ep, ev, re, r, rp, c, sg, og)
        local rpz = Duel.GetFieldCard(tp, LOCATION_PZONE, 1)
        local lscale = c:GetLeftScale()
        local rscale = rpz:GetRightScale()
        if lscale > rscale then lscale, rscale = rscale, lscale end
        local eset = {Duel.IsPlayerAffectedByEffect(tp, EFFECT_EXTRA_PENDULUM_SUMMON)}
        local tg = nil
        local loc = 0
        local ft1 = Duel.GetLocationCount(tp, LOCATION_MZONE)
        local ft2 = Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_PENDULUM)
        local ft = Duel.GetUsableMZoneCount(tp)
        local ect = c29724053 and Duel.IsPlayerAffectedByEffect(tp, 29724053) and c29724053[tp]
        if ect and ect < ft2 then ft2 = ect end
        if not Duel.IsExistingMatchingCard(aux.extramonsterzonefiter, tp, LOCATION_MZONE, 0, 1, nil) then
            ft2 = ft2 - 1
            ft = ft - 1
        end
        if Duel.IsPlayerAffectedByEffect(tp, 59822133) then
            if ft1 > 0 then ft1 = 1 end
            if ft2 > 0 then ft2 = 1 end
            ft = 1
        end
        if ft1 > 0 then loc = loc | LOCATION_HAND end
        if ft2 > 0 then loc = loc | LOCATION_EXTRA end
        if og then
            tg = og:Filter(Card.IsLocation, nil, loc):Filter(Auxiliary.PConditionFilter, nil, e, tp, lscale, rscale, eset)
        else
            tg = Duel.GetMatchingGroup(Auxiliary.PConditionFilter, tp, loc, 0, nil, e, tp, lscale, rscale, eset)
        end
        local ce = nil
        local b1 = Auxiliary.PendulumChecklist & (0x1 << tp) == 0
        local b2 = #eset > 0
        if b1 and b2 then
            local options = {1163}
            for _, te in ipairs(eset) do table.insert(options, te:GetDescription()) end
            local op = Duel.SelectOption(tp, table.unpack(options))
            if op > 0 then ce = eset[op] end
        elseif b2 and not b1 then
            local options = {}
            for _, te in ipairs(eset) do table.insert(options, te:GetDescription()) end
            local op = Duel.SelectOption(tp, table.unpack(options))
            ce = eset[op + 1]
        end
        if ce then tg = tg:Filter(Auxiliary.PConditionExtraFilterSpecific, nil, e, tp, lscale, rscale, ce) end
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
        Auxiliary.GCheckAdditional = Auxiliary.PendOperationCheck(ft1, ft2, ft)
        local g = tg:SelectSubGroup(tp, aux.TRUE, true, 1, math.min(#tg, ft))
        Auxiliary.GCheckAdditional = nil
        if not g then return end
        if ce then
            Duel.Hint(HINT_CARD, 0, ce:GetOwner():GetOriginalCode())
            ce:Reset()
        else
            Auxiliary.PendulumChecklist = Auxiliary.PendulumChecklist | (0x1 << tp)
        end
        sg:Merge(g)
        Duel.HintSelection(Group.FromCards(c))
        Duel.HintSelection(Group.FromCards(rpz))
    end
end

